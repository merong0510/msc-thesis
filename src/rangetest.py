# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 01:15:50 2019

@author: merong0510

"""


from __future__ import print_function
import torch
import torch.nn as nn

import torch.utils.data as data 
import torch.autograd as autograd
import torch.nn.init as weight_init
import torchvision.transforms as transforms
from torch.optim.optimizer import Optimizer, required


import os, sys
import numpy as np
import matplotlib.pyplot as plt
from torchvision.utils import save_image

global _DEVICE
_DEVICE=torch.device("cuda" if torch.cuda.is_available() else "cpu")


class PermutationLayer(autograd.Function):     
    @staticmethod
    def forward(ctx, x, p, coeff, RowCon, ColCon, DerivRow, DerivCol, SharpCon, DerivSharp):                
        ctx.save_for_backward(x, p, DerivCol, DerivRow, SharpCon, DerivSharp, coeff)
        return x.matmul(p), ColCon, RowCon, SharpCon, DerivSharp

    
    @staticmethod
    def backward(ctx, grad_output, Cconst, Rconst, Sharp, DerivSharp):
        
        x, p, DerCol, DerRow, Sharp, DerSharp, coeff  = ctx.saved_tensors

        grad_cnn = (grad_output*x.transpose(3,2)).sum(dim=0, keepdim=True)
        grad_col = grad_cnn + coeff[0]*DerCol
        grad_row = grad_cnn + coeff[1]*DerRow      
        
        #*set*#derivative of  sharpness      
#        grad_sharp = coeff[2]*DerSharp 
        
        if torch.isnan(grad_col.max()):
            print("grad_col is infinite")
            sys.exit()
        
        grad_p = grad_col + grad_row #+ grad_sharp

        return None, grad_p, None, None, None, None, None, None, None, None
    
    
    
    
    
    

PermLay = PermutationLayer.apply














class ApplyPermLayer(torch.nn.Module):
    def __init__( self, perm_size, num_perm, coeff):
        super(ApplyPermLayer, self).__init__()
        self.coeff = coeff    
        self.register_parameter("pm", torch.nn.Parameter(torch.randn(1, num_perm, perm_size, perm_size).sigmoid() * 0.1))
        self.mu = self.pm.sum(dim=3).max().data.int() + 1
        
#        save_image(self.pm[0,0,:,:], 'Initial_P_Matrix.png')
    
    def forward(self, input):
        RowConstraint, ColConstraint, RowConstraintder, ColConstraintder = self.Constraints()
        Sharpness, dSharpness = self.Sharping()
        return PermLay(input, self.pm, self.coeff, RowConstraint, ColConstraint, RowConstraintder, ColConstraintder, Sharpness, dSharpness)
    
    
    def Sharping(self):
        a = (1/(self.pm.max().item())**4)*(self.pm**2 - self.pm.max().item() * self.pm)**2
        da = (1/(self.pm.max().item())**4)*(self.pm**2 - self.pm.max().item() * self.pm)*(2*self.pm - self.pm.max().item())
                
        return a, da
    
    
    def Barrier(self, Rowc, Colc): #### just for each element's constraint
        Row = self.mu * (torch.log(1 + (1-Rowc)/self.mu))**2
        Col = self.mu * (torch.log(1 + (1-Colc)/self.mu))**2
        return Row.sum(dim=2, keepdim=True), Col.sum(dim=3, keepdim=True)    
    def Barrderiv(self, Rowc, Colc, Rowdsig, Coldsig):
        Row = -2 * (1/(1 + (1-Rowc)/self.mu)) * torch.log(1 + (1-Rowc)/self.mu)
        Col = -2 * (1/(1 + (1-Colc)/self.mu)) * torch.log(1 + (1-Colc)/self.mu)
        return Row,Col

    
    def Constraints(self): ## for doing backpropagation
        BarrierCol=0
        BarrierRow=0
        
        DerivBarrierCol=0
        DerivBarrierRow=0
        
        Rowsummed = self.pm.sum(dim=3, keepdim=True)
        Colsummed = self.pm.sum(dim=2, keepdim=True)
        Rowdersig = self.pm * (1 - self.pm).sum(dim=3, keepdim=True)
        Coldersig = self.pm * (1 - self.pm).sum(dim=2, keepdim=True)
        
        #Ci(x) = 1-Rowsummed , 1-Colsummed
        Rowv, Colv = self.Barrier(Rowsummed, Colsummed)
        derrows, dercols = self.Barrderiv(Rowsummed, Colsummed, Rowdersig, Coldersig)
        
        # backprop to PM weights
        BarrierCol+=Colv
        BarrierRow+=Rowv
        DerivBarrierCol+=dercols
        DerivBarrierRow+=derrows
        
        return BarrierRow, BarrierCol, DerivBarrierRow, DerivBarrierCol





class cNewton(Optimizer):
    def __init__(self, params):
        defaults = dict(lr=0)
        self.param_groups = []
        super(cNewton, self).__init__(params, defaults)
        
        param_groups = list(params)
        if len(param_groups) == 0:
            raise ValueError("optimizer got an empty parameter list")
            
        if not isinstance(param_groups[0], dict):
            param_groups = [{'params':param_groups}]
        
        
    def step(self, closure=None):
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    print("No p.grad!!")
                    sys.exit()
                    continue
                
                d_p = p.grad.data
                p.data.add_(-1, d_p)
#                print("cNewton",d_p.max())
        return loss
    
    def sharpstep(self, sgrad, omega, closure=None):
        loss = None
        if closure is not None:
            loss = closure()

        for group in self.param_groups:           
            for p in group['params']:
                if p.grad is None:
                    print("No p.grad!!")
                    sys.exit()
                    continue

                d_ps = sgrad.data

                p.data.add_(-omega, d_ps)

        return loss
            
    



class NET(torch.nn.Module):
    def __init__(self, num_perm, coeff):
        super(NET, self).__init__()
        
        ##do it seperately like following:
        self.PermLayer = ApplyPermLayer(perm_size=52, num_perm=num_perm, coeff=coeff)
        
        self.seq1 = nn.Sequential(
                ###################################################################
                torch.nn.Conv2d(num_perm, 16, kernel_size=(3,3), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(16),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU()
                )
        self.seq2 = nn.Sequential(
                                
                torch.nn.Conv2d(32, 48, kernel_size=(5,5), stride=1, padding=0 , bias=True),
                nn.BatchNorm2d(48),
                nn.MaxPool2d(kernel_size=3, stride=1),
                nn.ReLU(),
                )
        
        self.seq3 = nn.Sequential(
                
                nn.Linear(36864, 1000),
                nn.ReLU(),
                nn.Dropout(),
                
                nn.Linear(1000, 21),
                nn.ReLU()
                )
            
        
    def forward(self, x):
        
        out, ColBarr, RowBarr, Sharp, DerSharp = self.PermLayer(x) #,perm
        
        out = self.seq1(out)        
#        out = self.seq2(out)
   
        out = out.reshape(out.size(0), -1)
        
        out = self.seq3(out)
        

        return out, ColBarr, RowBarr, Sharp, DerSharp
    
    
    
    def init_weights(self):
        for m in self.modules():
            
            if type(m) is ApplyPermLayer:
                print('...initializing the PL values NOT DONE')
                print('======================================')
                       
            if type(m) is nn.Conv2d:
                print('...initializing the Conv2d.weights')
                
                for name, param in m.named_parameters():
                    if 'weight' in name:
                        weight_init.xavier_uniform_(param.data)
#                        weight_init.xavier_normal_(param.data)
                    if 'bias' in name:
                        weight_init.zeros_(param.data)
                        
 
            if type(m) is nn.Linear:
                print('...initializing the Linear.weights')
                
                for name, param in m.named_parameters():
                    if 'weight' in name:
                        weight_init.kaiming_normal_(param.data)
                    if 'bias' in name:
                        weight_init.zeros_(param.data)

    def PM_match(self, dim=0):
        if dim == 0:
            #argmax on rows
            global rowargmax
            ###########
            ###########
            rowargmax = self.CEasyargmax(self.PermLayer.pm.sigmoid())###############sigmoid_()#########3
                        
            # zero_init to self.pm
            self.PermLayer.pm = torch.nn.Parameter(torch.zeros((
                            self.PermLayer.pm.shape[0],self.PermLayer.pm.shape[1],
                            self.PermLayer.pm.shape[2],self.PermLayer.pm.shape[3]
                            ))
            )
            
            for channel in range(self.PermLayer.pm.shape[1]):
                print(channel)
                for row in range(self.PermLayer.pm.shape[2]):
                    print('row', row)
                    
                    print(rowargmax.shape, self.PermLayer.pm[0, channel, row,:].shape)
                    print('element',rowargmax[channel,row] )
                    self.PermLayer.pm[0, channel, row, rowargmax[channel,row].item()]=1
#                    self.PermLayer.pm[0, channel, row, rowargmax[channel,row].item()] = 1
            
        elif dim == 1:
            #argmax on cols
            colargmax = self.CEasyargmax(self.PermLayer.pm.transpose(3,2))
            # zero_init to self.pm
            self.PermLayer.pm = torch.nn.Parameter(torch.zeros((
                            self.PermLayer.pm.shape[0],self.PermLayer.pm.shape[1],
                            self.PermLayer.pm.shape[2],self.PermLayer.pm.shape[3]
                            ))
            )
            
            for channel in range(self.PermLayer.pm.size()[1]):
                for row in range(self.PermLayer.pm.size()[2]):
                    self.PermLayer.pm[0 ,channel, row, colargmax[channel,row].item()] = 1
        else:
            print("dim either 0 or 1.")
            sys.exit()
            
            
    def CEasyargmax(self, arr):
            
        mmidx = []    
        for ci, pm in enumerate(arr[0]):
            midx=[]
            for i in range(pm.size(0)):
                li = pm[i]
                max1, max2, min1, min2 = self.Range(li)
                a1 = li==max1
                a2 = li==max2
                index1 = (a1!=0).nonzero()
                index2 = (a2!=0).nonzero()
    #            print(i,'th i...........', pm.size(0))
    #            print(pm)
    #            print('index:', index1, index2.data)
                
                if (i+1) == pm.size(0):
    #                print('last row', li.nonzero())
                    lidx=li.nonzero()
    #                print(lidx)                
                    midx.append(lidx.item())
    #                print(midx)
                    break
    
                    
                if index1 not in midx:
                    midx.append(index1.item())
                    
                    if (pm[i+1][0] == True).item() == 0:
                        pm[:,index1]=0
    
                    elif (pm[i+1][0] == True).item() == 1:
    #                    print('no upcoming row')
                        sys.exit()
                    
    
                elif index1 in midx:
                    if index2 not in midx:
                        midx.append(index2.item())
                    elif index2 in midx:
                        print('inside', index1.item(), midx)
                        sys.exit()
                    continue
            mmidx.append(np.asarray(midx))
        return torch.tensor(np.asarray(mmidx))


    def Range(self, list1):  
        largest = list1[0]   
        largest2 = 0
        lowest = list1[0]
        lowest2 = 0
    
        for item in list1[1:]:      
            if item > largest:  
                largest2 = largest 
                largest = item  
            elif largest2 == 0 or largest2 < item:  
                largest2 = item
            if item < lowest:  
                lowest2 = lowest 
                lowest = item
            elif lowest2 == 0 or lowest2 > item:  
                lowest2 = item
        return largest, largest2, lowest, lowest2


#    def PM_match(self, dim = 0):
#        argmaxrow = self.Cargmax(self.pm.transpose(3,2))
#        argmaxcol = self.Cargmax(self.pm)
#        
#        if dim == 0:
#            #rowwise
#            for i,_i in enumerate(argmaxrow):
#                self.pm[:,i]=0
#                self.pm[argmaxrow[i],i] = 1
#            
#        elif dim == 1:
#            #colwise
#            for i,_i in enumerate(argmaxcol):
#                self.pm[i,:]=0
#                self.pm[i,argmaxcol[i]] = 1
#        else:
#            print("dim either 0 or 1.")
#            sys.exit()
                
#
#    def Cargmax(self, arr): 
#        ##in snippets
#        print('NOT YET')
#        sys.exit()
#        midx=[]              
#        return np.asarray(midx)




class SaveFeatures():
    def __init__(self, module):
        self.hook = module.register_forward_hook(self.hook_fn)
    def hook_fn(self, module, input, output):
        self.features = torch.tensor(output,requires_grad=True).cuda()
    def close(self):
        self.hook.remove()




C1=0.5*0.002
C2=0.5*0.002
S=0.001


num_epochs = 5
batch_size = 100
learning_rate = torch.tensor([0.00001], requires_grad=False)
num_perm = 10
# Col, Row, Sharpness
coeff = torch.tensor([C1,C2,S], requires_grad=False)
omega = torch.tensor([0.001, 0.003, 0.006, 0.008, 0.012], requires_grad=False)



#from tempTE_dataset_Rdata import TE_process
#
#trans = None
#trainset = TE_process(train=True, transform=trans, target_transform=trans)
#train_loader = data.DataLoader(trainset, batch_size=batch_size,
#                                         shuffle=True, num_workers=0)
#sys.exit()

#testset = TE_process(train=False, transform=trans, target_transform=trans)
#test_loader = data.DataLoader(testset, batch_size=batch_size,
#                                         shuffle=True, num_workers=0)





model = NET(num_perm, coeff)
#model.to(_DEVICE)
model.init_weights()

print('Start sum: ', model.PermLayer.pm[0,0,:,:].sum(dim=1)[0:10], '\n')


# Loss and optimizer
criterion = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.SGD([
                {'params': model.seq1.parameters()},
                {'params': model.seq2.parameters()},
                {'params': model.seq3.parameters()},
                ], lr=0.001)


c_optimizer = cNewton([
                {'params':model.PermLayer.parameters()}
                ])

is_cuda_available = torch.cuda.is_available();



# Train the model
total_step = len(train_loader)
print('the length of the train_loader: ', int(len(train_loader)))


loss_list = []
sloss_list = []
bloss_list = []
acc_list = []

test=[]
test2=[]

omegacount = 0

for epoch in range(num_epochs):
    for i, (images, labels) in enumerate(train_loader):

        if images.shape[0]!=batch_size:
            continue
#        images=images.to(_DEVICE)
#        labels=labels.to(_DEVICE)
        
        out, ColBar, RowBar, Sharp, derSharp = model(images)
        
        
        outputs = out
        loss = criterion(out, labels)
        
        loss_sharp = Sharp.sum()
        loss_barrierc = ColBar.sum()
        loss_barrierr = RowBar.sum()
        
        loss_barrier = loss_barrierc + loss_barrierr
        
#        print('start', ColBar, RowBar)
#        print('barrier loss==', loss_barrier.item(), 'sum===',model.PermLayer.pm[0,0,:,:].sum(dim=1).max().item(),'\n')
        c_loss = loss_barrier#+loss_sharp
        
        sloss_list.append(loss_sharp.item())
        bloss_list.append(loss_barrier.item())
        loss_list.append(loss.item())
        
        
        #zero the parmeter gradients
        optimizer.zero_grad()
        c_optimizer.zero_grad()
        
        loss.backward(retain_graph=True)
        c_loss.backward()
#        print('grad before optimizer',model.PermLayer.pm.max().item())
#        print(model.PermLayer.pm.grad.max().item())
        #updating the convolutional layer        
        optimizer.step()
        #updating the Permutaion layer
#        print('grad after optimizer',model.PermLayer.pm.max().item())
#        print(model.PermLayer.pm.grad.max().item())
        c_optimizer.step()
#        print('grad AFTER BARRIER',model.PermLayer.pm.max().item())
#        print(model.PermLayer.pm.grad.max().item())
        
        # without the .backward() bcs of the different initiation timing
        # Since the operation is done to the pm directly, the p.grad is not necessary.
        c_optimizer.sharpstep(derSharp, omega[omegacount])
#        print('grad AFTER SHARPENING',model.PermLayer.pm.max().item())
#        print(model.PermLayer.pm.grad.max().item())
#        sys.exit()

#        print(i%round(len(train_loader)/len(omega)), 'current omega', omega[omegacount])
        if i%round(len(train_loader)/len(omega)) == 0 and omegacount < len(omega):
            if i == 0:
                continue
            omegacount += 1
            print('plus omega in ', i ,'to', omega[omegacount] ,'omegacount', omegacount)

                
        
        test.append(model.PermLayer.pm.max().item())
        test2.append(model.PermLayer.pm.min().item())
        
        total = labels.size(0)
        _, predicted = out.data.max(1)                         ########## 1. whether the outputs ( outputs.max() ) consistent with prediction.    2. prediction and labels-> correct  affects or not?

        correct = (predicted == labels).sum().item()
        acc_list.append(correct / total)
        sys.exit()

        if (i + 1) % 10 == 0:
            print('\n Epoch [{}/{}], Step [{}/{}], Accuracy: {:.2f}%, \
                  Barrier loss: {:.4f}, System Loss: {:.4f}, '
                  .format(epoch + 1, num_epochs, i + 1, total_step, (correct / total) * 100, 
                          loss_barrier.item(), loss.item()))
            
    omegacount = 0


model.PM_match(dim=0)



test = torch.randn(1,6,25,25, dtype=torch.float)
test.sigmoid_()

out = CEasyargmax(test)


'''
test_losses = []
tbarr_losses = []
ts_losses = []

model.eval()
correct = 0 
with torch.no_grad():
        
    for ti, (img, target) in enumerate(test_loader):
            if img.shape[0]!=batch_size:
                continue
            
            output, tColBar, tRowBar, tsharpness = model(img)
            
            test_loss = criterion(out, target)
            
            test_losses.append(test_loss.item())
            tbarr_losses.append(tColBar.sum().item() + tRowBar.sum().item())
            ts_losses.append(tsharpness.sum())
            
            pred = output.data.max(1, keepdim=False)[1]
            
            batch_total = target.size(0)
            batch_correct = (pred == target).sum().item()
            
            correct += pred.eq(target.data.view_as(pred)).sum()
            print("Step [{}/{}], Loss:{:.4f}, Acc:{:.2f}%" .format(ti+1, len(test_loader), test_loss, (batch_correct / batch_total)*100))
    
    test_losses = np.asarray(test_losses).mean()
    print("\nTest set: Avg. loss: {:.4f}, Acc: {}/{} ({:.2f}%)\n" .format(test_losses, correct, len(test_loader.dataset), 100.* correct / len(test_loader.dataset) ))
'''

            
'''
test_losses = []

model.eval()
correct = 0 
with torch.no_grad():
    for ti, (img, target) in enumerate(test_loader):
        output = model(img)
        test_loss = criterion(output, target).item()
        test_losses.append(test_loss)
        pred = output.data.max(1, keepdim=False)[1]
        
        batch_total = target.size(0)
        batch_correct = (pred == target).sum().item()
        
        correct += pred.eq(target.data.view_as(pred)).sum()
        
        print("Step [{}/{}], Loss:{:.4f}, Acc:{:.2f}%" .format(ti+1, len(test_loader), test_loss, (batch_correct / batch_total)*100))
    
    test_losses = np.asarray(test_losses).mean()
    print("\nTest set: Avg. loss: {:.4f}, Acc: {}/{} ({:.2f}%)\n" .format(test_losses, correct, len(test_loader.dataset), 100.* correct / len(test_loader.dataset) ))



'''

#####################Lr range test
#import math
#
#end_lr = 0.01
#start_lr = learning_rate
#lr_find_epochs = 3
###Learning rate
#lr_lambda = lambda x: math.exp(x * math.log(end_lr / start_lr)/(lr_find_epochs * len(train_loader)))
#scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda)
#
### lists to capture the logs of Cyclical learning Rates
#lr_find_loss = []
#lr_find_lr = []
#iter = 0
#smoothing = 0.1
#
#acc_list=[]
##for epoch in range(num_epochs):
#for j in range(lr_find_epochs):
#    print("lr_find epoch: {}".format(j))
#    for i, (images, labels) in enumerate(train_loader):
#    
#        if images.shape[0]!=batch_size:
#            continue
#        
#        #forward+backward+optimize
#        outputs = model(images)
#        
#        labels.view(-1)
#        loss = criterion(outputs, labels)
#        
#        #zero the parmeter gradients
#        optimizer.zero_grad()
#        loss.backward()
#        optimizer.step()
#        
#        
#        if SLR == True:
#            scheduler.step()
#            
#            lr_step = optimizer.state_dict()["param_groups"][0]["lr"]
#            lr_find_lr.append(lr_step)
#            
#            
#            if iter == 0:
#                lr_find_loss.append(loss.item())
#            else:
#                loss = smoothing * loss + (1-smoothing)*lr_find_loss[-1]
#                lr_find_loss.append(loss.item())
#            iter+=1
#            
#            
#            print(' Step [{}/{}], Loss: {:.4f}, lr_find_lr: {:.4f}'
#              .format( i + 1, total_step, loss.item(), lr_step))
#            
#            
#            #### criterion
#    #        if (correct/total) >= acc_list[-2]: # whether [-2] is right should be checked
#                #select the learing rate.     
#        
#        
#        total = labels.size(0)
#        _, predicted = torch.max(outputs.data, 1)                         ########## 1. whether the outputs ( outputs.max() ) consistent with prediction.    2. prediction and labels-> correct  affects or not?
#        
#        correct = (predicted == labels).sum().item()
#        acc_list.append(correct / total)
#        
##        if (i + 1) % 50 == 0:
#        print(' Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}% '
#              .format( i + 1, total_step, loss.item(), (correct / total) * 100))
#        
#        
#plt.plot(np.asarray(lr_find_lr), np.asarray(lr_find_loss))
#        







####################Lr range test
import math

end_lr = 0.01
start_lr = learning_rate
lr_find_epochs = 3
##Learning rate
lr_lambda = lambda x: math.exp(x * math.log(end_lr / start_lr)/(lr_find_epochs * len(train_loader)))
scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda)

## lists to capture the logs of Cyclical learning Rates
lr_find_loss = []
lr_find_lr = []
iter = 0
smoothing = 0.1
acc_list
for j in range(lr_find_epochs):
    print("lr_find epoch: {}".format(j))
    for i, (images, labels) in enumerate(train_loader):
        model.train()
        if images.shape[0]!=batch_size:
            continue
        optimizer.zero_grad()
        c_optimizer.zero_grad()
        
        #forward+backward+optimize
        out, ColBar, RowBar, Sharp, derSharp = model(images)
        loss_barrierc = ColBar.sum()
        loss_barrierr = RowBar.sum()
        
        loss_barrier = loss_barrierc + loss_barrierr
        
        labels.view(-1)
        loss = criterion(out, labels)
        c_loss = loss_barrier#+loss_sharp
        loss_list.append(loss.item())
        
        loss.backward(retain_graph=True)
        c_loss.backward()
        
        optimizer.step()
        c_optimizer.step()  
        c_optimizer.sharpstep(derSharp, omega[omegacount])
        
        scheduler.step()
        lr_step = optimizer.state_dict()["param_groups"][0]["lr"]
        lr_find_lr.append(lr_step)
        
        # smooth the loss
        if iter == 0 :
            lr_find_loss.append(loss)
        else:
            loss = smoothing*loss + (1-smoothing)*lr_find_loss[-1]
            lr_find_loss.append(loss)
            
        iter += 1
        
        if i%round(len(train_loader)/len(omega)) == 0 and omegacount < len(omega):
            if i == 0:
                continue
            omegacount += 1
        
        total = labels.size(0)
        _, predicted = torch.max(outputs.data, 1)                         ########## 1. whether the outputs ( outputs.max() ) consistent with prediction.    2. prediction and labels-> correct  affects or not?
        
        correct = (predicted == labels).sum().item()
        acc_list.append(correct / total)
        
#        if (i + 1) % 50 == 0:
        print(' Step [{}/{}], Loss: {:.4f}, Accuracy: {:.2f}% , lr:{} '
              .format( i + 1, total_step, loss.item(), (correct / total) * 100, lr_step))


plt.plot(np.asarray(lr_find_lr),np.asarray(lr_find_loss))

 
