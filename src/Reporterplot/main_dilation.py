import torch
import xlrd
import xlwt
from xlutils.copy import copy
import math
import time
import sys
sys.path.append('/home/at-lab/ownCloud/02_NAS')
sys.path.append('/media/NAS/Reimann')
import HelperFunctions as HelpFunc
import MathFunctions as MathFunc
import os
import matplotlib.pyplot as plt
from Classes import GenDilation as cConv2d
import math
import dill
torch.backends.cudnn.benchmark=True
plt.ioff()

class DummyNet(torch.nn.Module):
    def __init__(self, ExcelCol):
        super(DummyNet, self).__init__()
        self.SEQUENCE=torch.nn.Sequential()
        self.COL=ExcelCol
        self.SHEET=0
        self.NAME=0
        self.BOOK=0
        self.FOLDER=0
        self.KEEPTRAIN=0
        self.NROWS=0
        self.MASKS=[]
        
    def forward(self, x):
        return self.SEQUENCE(x)

    def AddLayer(self, layertype, layer):
        self.SEQUENCE.add_module(str(len(self.SEQUENCE)+1)+"_"+layertype, layer)
    
    def Start(self):
        self.WriteTimeStart()
        for layer in self.SEQUENCE:
            if isinstance(layer, cConv2d):
                self.MASKS.append(layer.mask*1.0)            
        
        
    def WriteTimeStart(self):
        self.SHEET.write(2, self.COL, time.strftime("%y_%m_%d__%H_%M_%S"))
        
    def WriteTimeEnd(self, retrain=True):
        self.SHEET.write(3, self.COL, time.strftime("%y_%m_%d__%H_%M_%S"))
        if not self.KEEPTRAIN:
            self.SHEET.write(4, self.COL, 0)
            
    def WriteFolder(self):
        XlWrite(self.SHEET, 23, self.COL, "file:///"+self.FOLDER, "linux", link=True)
        XlWrite(self.SHEET, 24, self.COL, "Z:\\400__Server\\Reimann\\01_Dilation\\00_Train\\"+time_str+"\\", "windows", link=True)
    
    def WriteResults(self, reporter):
        lasttrainloss=float(reporter.VALS["avg. Loss"][-1])
        lasttesloss=float(reporter.VALS["Test Loss"][-1])
        
        lasttrainacc=float(reporter.VALS["avg. Acc"][-1])
        lastestacc=float(reporter.VALS["Test Acc"][-1])
        
        XlWrite(self.SHEET, 19, self.COL, lastestacc, lastestacc)
        XlWrite(self.SHEET, 20, self.COL, lasttesloss, lasttesloss)
        XlWrite(self.SHEET, 21, self.COL, lasttrainacc, lasttrainacc)
        XlWrite(self.SHEET, 22, self.COL, lasttrainloss, lasttrainloss)
            
        
class D2toD1_class(torch.nn.Module):
    def __init__(self, num_pixels):
        super(D2toD1_class, self).__init__()
        self.num_pixels = num_pixels

    def forward(self, x):
        return x.view(-1, self.num_pixels)   
    
    def __repr__(self):
        return("2Dto1D(out_shape: %i)"%(self.num_pixels))
        
class D1toD2_class(torch.nn.Module):
    def __init__(self, channels, pixels):
        super(D1toD2_class, self).__init__()
        self.channels=channels
        self.pixels=pixels

    def forward(self, x):
        return x.view(-1, self.channels, self.pixels, self.pixels)     
    
    def __repr__(self):
        return("2Dto1D(out_shape: (-1,%i,%i,%i))"%(self.channels, self.pixels))   
    
def ConvFromString(string, size, channels):
    kernel_size=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    kernel_num=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    stride=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    padding=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    dilation=int(string)
    layer=torch.nn.Conv2d(in_channels=channels,
                    out_channels=kernel_num,
                    kernel_size=kernel_size,
                    stride=stride,
                    padding=padding,
                    dilation=dilation)
    outsize=math.ceil(int(size-kernel_size+2*padding)/stride+1)
    outchannels=kernel_num
    
    return layer, outsize, outchannels
    
def CustomConvFromString(string, size, channels):
    kernel_size=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    field_size=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    kernel_num=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    stride=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    padding=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    coeff_exp=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    coeff_lin=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    coeff_max=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    rows=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    cols=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    alls=float(string)
    
    layer=cConv2d(in_channels=channels,
                  out_channels=kernel_num,
                  kernel_size=kernel_size,
                  stride=stride,
                  padding=padding,
                  field_size=field_size,
                  coeff_exp=coeff_exp,
                  coeff_lin=coeff_lin,
                  coeff_max=coeff_max,
                  rows=rows,
                  cols=cols,
                  alls=alls)
    outsize=math.ceil(int(size-field_size+2*padding)/stride+1)
    outchannels=kernel_num
    
    return layer, outsize, outchannels

def PermutationFromString(string, size, channels):
    kernel_size=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    field_size=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    kernel_num=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    stride=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    padding=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    coeff_exp=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    coeff_lin=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    coeff_max=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    rows=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    cols=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    alls=float(string)
    
    layer=cConv2d(in_channels=channels,
                  out_channels=kernel_num,
                  kernel_size=kernel_size,
                  stride=stride,
                  padding=padding,
                  field_size=field_size,
                  coeff_exp=coeff_exp,
                  coeff_lin=coeff_lin,
                  coeff_max=coeff_max,
                  rows=rows,
                  cols=cols,
                  alls=alls)
    outsize=math.ceil(int(size-field_size+2*padding)/stride+1)
    outchannels=kernel_num
    
    return layer, outsize, outchannels

def BatchnormFromString(size, channels, params):
    eps=float(params[:params.find(",")])
    mom=float(params[params.find(",")+1:])
    layer=torch.nn.BatchNorm2d(num_features=channels, eps=eps, momentum=mom)
    return layer, size, channels

def Pool2dFromString(string, size, channels):
    kernel=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    stride=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    padding=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    dilation=int(string)
    
    
    layer=torch.nn.MaxPool2d(kernel, stride, padding, dilation)
    return layer, math.floor(size/kernel), channels
   
def D2toD1(size, channels):
    size=channels*size*size
    channels=1
    layer=D2toD1_class(size)
    return layer, size, channels
    
def FcFromString(string, size, channels):
    layer=torch.nn.Linear(in_features=size,
                          out_features=int(string))
    size=int(string)
    channels=1
    return layer, size, channels

def Drop1dFromString(string, size, channels):
    layer=torch.nn.Dropout(p=float(string))
    return layer, size, channels

def Drop2dFromString(string, size, channels):
    layer=torch.nn.Dropout2d(p=float(string))
    return layer, size, channels

def IdentifyLayer(string, size, channels):
    string=string.lower()
    
    layertype=string[:string.find("(")]
    match=False
    if layertype=="conv":
        layertype="Layer_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=ConvFromString(string_args, size, channels)
        match=True
    elif layertype=="customconv":
        layertype="Layer_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=CustomConvFromString(string_args, size, channels)
        match=True
    elif layertype=="fc":
        layertype="Layer_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=FcFromString(string_args, size, channels)
        match=True
    elif layertype=="batchnorm2d":
        layertype="Norm_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=BatchnormFromString(size, channels, string_args)
        match=True
    elif layertype=="pool2d":
        layertype="Norm_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=Pool2dFromString(string_args, size, channels)
        match=True
    elif layertype=="drop1d":
        layertype="Shape_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=Drop1dFromString(string_args, size, channels)
        match=True
    elif layertype=="drop2d":
        layertype="Shape_"+layertype
        string_args=string[string.find("(")+1:string.find(")")]
        layer, outsize, outchannels=Drop2dFromString(string_args, size, channels)
        match=True
    elif layertype=="2dto1d":
        layertype="Shape_"+layertype
        layer, outsize, outchannels=D2toD1(size, channels)
        match=True
    elif layertype=="mynewfunc":
        layertype="Shape_"+layertype
        layer, outsize, outchannels=D2toD1(size, channels)
        match=True
    elif layertype=="permutation":
        layertype="Layer_"+layertype
        layer, outsize, outchannels=D2toD1(size, channels)
        match=True
    if not match:
        print("=== %s not found as Layertype==="%(layertype))
    return layertype, layer, outsize, outchannels

def IdentifyActivationFunction(string):
    string=string.lower()
    layertype=-1
    layer=False

    if string.find(";")>0:
        end=string[string.find(";")+1:]
        if end.find("(")>-1:
            name=end[:end.find("(")]
            params=end[end.find("(")+1:-1]
        else:
            name=end
        match=False
        
        layertype="ActFcn"
        if name=="linear":
            layer=-1
            layertype+="_Linear"
            match=True
        elif name=="leaky":
            layer=torch.nn.LeakyReLU()
            layertype+="_LeakyReLU"
            match=True
        elif name=="relu":
            layer=torch.nn.ReLU()
            layertype+="_ReLU"
            match=True
        elif name=="sigmoid":
            layer=torch.nn.Sigmoid()
            layertype+="_Sigmoid"
            match=True
        elif name=="tanh":
            layer=torch.nn.Tanh()
            layertype+="_Tanh"
            match=True
        elif name=="swish":
            beta=float(params)
            layer=MathFunc.SwishModule(beta)
            layertype+="_Swish"
            
            match=True
        if not match:
            print("=== %s not found as Activation Function==="%(layertype))
    return layertype, layer

def CreateOptimizer(net, string):
    string=string.lower()
    
    name=string[:string.find("(")]
    params=string[string.find("(")+1:string.find(")")]
    
    if name=="adam":
        lr=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        beta1=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        beta2=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        eps=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        decay=float(params)
        
        optimizer=torch.optim.Adam(net.parameters(), lr=lr, betas=(beta1, beta2), eps=eps, weight_decay=decay)
        
    elif name=="sgd":
        lr=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        mom=float(params[:params.find(",")])
        
        optimizer=torch.optim.SGD(net.parameters(), lr=lr, momentum=mom)
        
    elif name=="adadelta":
        lr=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        rho=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        eps=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        decay=float(params)
        
        optimizer=torch.optim.Adadelta(net.parameters(), lr=lr, rho=rho, eps=eps, weight_decay=decay)
        
    elif name=="adagrad":
        lr=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        lr_decay=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        weight_decay=float(params[:params.find(",")])
        params=params[params.find(",")+1:]
        
        eps=float(params)
        
        optimizer=torch.optim.Adagrad(net.parameters(), lr=lr, lr_decay=lr_decay, weight_decay=weight_decay, eps=eps)
        
    return optimizer

def CreateScheduler(optimizer, string):
    string=string.lower()
    
    name=string[:string.find("(")]
    
    if name=="exp":
        string_args=string[string.find("(")+1:string.find(")")]
        gamma=float(string_args)
        scheduler=torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma=gamma)
    elif name=="step":
        string_args=string[string.find("(")+1:string.find(")")]
        gamma=float(string_args[:string_args.find(",")])
        
        string_args=string_args[string_args.find(",")+1:]
        
        if string_args.find(",")>0:
            # multi steps
            milestones=[]
            while string_args.find(",")>0:
                milestones.append(int(string_args[:string_args.find(",")]))
                string_args=string_args[string_args.find(",")+1:]
            milestones.append(int(string_args))
            scheduler=torch.optim.lr_scheduler.MultiStepLR(optimizer, milestones=milestones, gamma=gamma)
        else:
            # fixed stepsize
            step_size=float(string_args)
            scheduler=torch.optim.lr_scheduler.StepLR(optimizer, step_size=step_size, gamma=gamma)
    return scheduler    

def ReporterFromString(string):    
    step=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    avg=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    show=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    line=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    header=int(string)
    
    names=["Minibatch", "Acc", "Loss", "Test Acc", "Test Loss"]
    reporter=HelpFunc.DynReport(names=names,
                                precision=3,
                                average=avg,
                                show=show,
                                line=line,
                                header=header,
                                autoshow=True,
                                step=step)
    return reporter

def AugmenterFromString(string):
    crop_range=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    flip_x=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    flip_y=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    shift_x=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    shift_y=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    rot_angle=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    zca=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    noise_peak=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    exponent=float(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    normalize=int(string[:string.find(",")])
    string=string[string.find(",")+1:]
    
    extend=int(string)
    
    augmenter=HelpFunc.AugmentMinibatch(crop_range=crop_range,
                                        flip_x=flip_x,
                                        flip_y=flip_y,
                                        shift_x=shift_x,
                                        shift_y=shift_y,
                                        rot_angle=rot_angle,
                                        zca=zca,
                                        noise_peak=noise_peak,
                                        exponent=exponent,
                                        normalize=normalize,
                                        extend=extend)
    return augmenter

def InitializerFromString(string, gain):
    # gain
    if gain.find("(")>-1:
        gainname=gain[:gain.find("(")]
        params=float(gain[gain.find("(")+1:-1])
    else:
        gainname=gain
        
    if gainname=="linear":
        gain=torch.nn.init.calculate_gain("linear")
    elif gainname=="sigmoid":
        gain=torch.nn.init.calculate_gain("sigmoid")
    elif gainname=="conv":
        gain=torch.nn.init.calculate_gain("conv")
    elif gainname=="tanh":
        gain=torch.nn.init.calculate_gain("tanh")
    elif gainname=="relu":
        gain=torch.nn.init.calculate_gain("relu")
    elif gainname=="leaky":
        gain=torch.nn.init.calculate_gain("leaky_relu", params)
        
    # init type
    if string.find("(")>-1:
        name=string[:string.find("(")]
        params=string[string.find("(")+1:-1]
    else:
        name=string
    
    if name=="xavier_uniform":
        initializer=lambda tensor: torch.nn.init.xavier_uniform_(tensor, gain)
        
    elif name=="xavier_normal":
        initializer=lambda tensor: torch.nn.init.xavier_normal_(tensor, gain)
        
    elif name=="kaiming_uniform":
        a=float(params[:params.find(",")])
        mode=params[:-1]
        initializer=lambda tensor: torch.nn.init.kaiming_uniform_(tensor, a, mode, gainname)
        
    elif name=="kaiming_normal":
        initializer=lambda tensor: torch.nn.init.kaiming_normal_(tensor, a, mode, gainname)
        
    elif name=="uniform":
        low=float(params[:params.find(",")])
        high=float(params[params.find(",")+1:])
        initializer=lambda tensor: torch.nn.init.uniform_(tensor, low, high)
        
    elif name=="normal":
        mean=float(params[:params.find(",")])
        std=float(params[params.find(",")+1:])
        initializer=lambda tensor: torch.nn.init.normal_(tensor, mean, std)
    return initializer

def L1Func(net):
    retval=0
    for param in net.parameters():
        retval+=param.abs().sum()
    return retval

def L2Func(net):
    retval=0
    for param in net.parameters():
        retval+=(param**2).sum()
    return retval**0.5

def NormFromString(L1, L2):
    norm=lambda net: float(L1)*L1Func(net) + float(L2)*L2Func(net)
    return norm

def LoadNets(path):
    NetList=[]
    Optimizers=[]
    Schedulers=[]
    Augmenters=[]
    NetNums=[]
    Parameters=[]
    
    data=xlrd.open_workbook(path).sheet_by_index(0)
    
    # loop over all rows
    for netnum in range(1,data.ncols):
        netdata= lambda row: data.cell_value(row,netnum)
        
        retrain=netdata(4)
        if retrain:
            number=str(netdata(0))
            name=str(netdata(1))
            
            print("Found net \"%s\" (Run %s) to train in column %i."%(name, number, netnum))
            net=DummyNet(netnum)
            net.NROWS=data.nrows
            net.NAME=netdata(1)
            net.KEEPTRAIN=netdata(5)
            dataset=netdata(6).lower()
            
            if dataset=="mnist":
                inputsize=28
                inputchannels=1
                print("\tLoading MNIST Dataset...")
                _,_,trainer, tester=HelpFunc.LoadMnist(int(netdata(10)))
                print("\tLoading MNIST Dataset -DONE-")
            elif dataset=="cifar10":
                inputsize=32
                inputchannels=3
                print("\tLoading Cifar10RGB Dataset...")
                _,_,trainer, tester=HelpFunc.LoadCifar10RGB(int(netdata(10)))
                print("\tLoading Cifar10RGB Dataset -DONE-")
            
            print("\tCreating Network layers...")
            for layernum in range(25,data.nrows):
                if netdata(layernum)=="":
                    break
                layertype, layer, inputsize, inputchannels = IdentifyLayer(netdata(layernum), inputsize, inputchannels)
                print("\t\t%s added..."%(layertype))
                net.AddLayer(layertype, layer)
                
                layertype, layer = IdentifyActivationFunction(netdata(layernum))
                if layer:
                    print("\t\t%s added..."%(layertype))
                    net.AddLayer(layertype, layer)
            
            if netdata(15)!=0:
                initializer=InitializerFromString(netdata(15), netdata(16))
                
                for paramname, val in net.named_parameters():
#                    print(paramname)
                    if paramname[-15:]=="customconv.mask":
                        continue
                    if len(val.shape)>=2:
                        initializer(val)
                        
            print("\tCreating Network layers -DONE-")
            
            print("\tCreating Optimzer, Scheduler, Augmenter and Reporter...")
            # Optimizer
            optimizer=CreateOptimizer(net, netdata(7))
            
            # Scheduler
            scheduler=CreateScheduler(optimizer, netdata(8))
            
            # Augmenter
            augmenter=AugmenterFromString(netdata(13))
                    
            # Reporter and Custom Paramters
            parameter={}
            parameter["reporter"]=ReporterFromString(netdata(12))
            parameter["ep"]=int(netdata(9))
            parameter["trainer"]=trainer
            parameter["tester"]=tester
            parameter["testevery"]=netdata(11)
            parameter["criterion"]=CriterionFromString(netdata(14))
            parameter["norm"]=NormFromString(netdata(17), netdata(18))
            print("\tCreating Optimzer, Scheduler, Augmenter and Reporter -DONE-")
            
            net.NAME=name
            net=net.to(_DEVICE)
            NetList.append(net)
            Optimizers.append(optimizer)
            Schedulers.append(scheduler)
            Augmenters.append(augmenter)
            NetNums.append(number)
            Parameters.append(parameter)
            print("\tNetwork \"%s\" (Run %s) fully prepared"%(name, number))
    return NetList, Optimizers, Schedulers, Augmenters, NetNums, Parameters

def CriterionFromString(string):
    string=string.lower()
    if string=="crossentropy" or "ce":
        criterion=torch.nn.CrossEntropyLoss()
    elif string=="mse":
        criterion=torch.nn.MSELoss()
    return criterion

def OpenExcel(path):
    wb_original=xlrd.open_workbook(path)
    wb_copy=copy(wb_original)
    wb_sheet=wb_copy.get_sheet(0)

    return wb_copy, wb_sheet

def TestNet(net, tester, criterion):
    correct=0
    samples=0
    loss=0
    batch=0
    
    net.eval()
    for _, (data, label) in enumerate(tester):
        data=data.to(_DEVICE)
        label=label.to(_DEVICE)
        outs=net(data)
        correct+=torch.sum(torch.argmax(outs, dim=1)==label)
        samples+=data.shape[0]
        loss+=criterion(outs, label.long()).item()
        batch+=1
    net.train()
    return float(100.0*correct)/samples, loss/batch

def TrainLoop(net, optimizer, scheduler, augmenter, parameters):
    net.train()
    
    trainer=parameters["trainer"]
    tester=parameters["tester"]
    criterion=parameters["criterion"]
    epmax=parameters["ep"]
    reporter=parameters["reporter"]
    testevery=parameters["testevery"]
    norm=parameters["norm"]
    
    net.Start()
    
    batch=0
    t_start=time.time()
    for ep in range(epmax):
        for _, (data, label) in enumerate(trainer):
            data=data.to(_DEVICE)
            label=label.to(_DEVICE)
            data=augmenter.Apply(data)
            optimizer.zero_grad()
            label=label.long()
            outs=net(data)
            
            loss=criterion(outs, label)+norm(net)
            loss.backward()
            optimizer.step()
            
            train_acc=100.0*float(torch.sum(torch.argmax(outs, dim=1)==label))/data.shape[0]
            
            if batch%testevery==0:
                lasttestloss, lasttestacc=TestNet(net, tester, criterion)
                
            if batch%reporter.STEP==0:
                reporter.SetValues([batch, train_acc, loss.item(), lasttestloss, lasttestacc])
            batch+=1
        scheduler.step()
        lasttestloss, lasttestacc=TestNet(net, tester, criterion)
    print("\n------ Last Values:")
    reporter.Show()
    print("\n------")
    reporter.DUMPDICT["SecPerEpoch"]=(time.time()-t_start)/epmax
    net.WriteTimeEnd(retrain=True)
    net.WriteResults(reporter)
    
    return data

def XlWrite(sheet, row, col, value, name, link=False):
    if link:
        sheet.write(row, col, xlwt.Formula('HYPERLINK("%s";"%s")'%(str(value), str(name))))
    else:
        sheet.write(row, col, value)

def XlSave(book):
    while True:
        try:
            book.save(_FILENAME)
            break
        except OSError:
            print("Excel file Busy, trying to save every 5 seconds")
            time.sleep(5)

        
###################################################################################################
###################################################################################################
###################################################################################################
if __name__ == "__main__":
    time_str=time.strftime("%y_%m_%d__%H_%M_%S")
    folder=os.getcwd()+"/"
    folder_train=folder+"00_Train"+"/"+time_str+"/"
        
        
    global _FILENAME, _COLORS, _DEVICE
    _FILENAME=folder+"ExperimentsDilation.xls"
    _COLORS=["royalblue", "maroon", "green", "black", "orangered", "gray", "teal", "purple", "goldenrod", "mediumslateblue"]
    _DEVICE="cuda" if torch.cuda.is_available() else "cpu"
    #_DEVICE="cpu"
    
    
    xl_book, xl_sheet=OpenExcel(_FILENAME)
    netlist, optimizers, schedulers, augmenters, netnumbers, parameters=LoadNets(_FILENAME)
    
    if len(netlist)>6:
        print("Too many nets. Stopping.\n Current: %i\n Max:     %i"%(len(netlist), 6))
        sys.exit()
    if len(netlist)>0:
        if not os.path.exists(folder_train):
            os.makedirs(folder_train)
            
        for net in netlist:
            net.SHEET=xl_sheet
            net.BOOK=xl_book
            net.FOLDER=folder_train
        
        for netidx in range(len(netlist)):
            net=netlist[netidx]
            optimizer=optimizers[netidx]
            scheduler=schedulers[netidx]
            augmenter=augmenters[netidx]
            netnumber=netnumbers[netidx]
            parameter=parameters[netidx]
            print("Start train net %i of %i (%s)"%(netidx+1, len(netlist), net.NAME))
            inputs=TrainLoop(net, optimizer, scheduler, augmenter, parameter)
            net.WriteFolder()
            XlSave(xl_book)
        figures, figurenames = HelpFunc.PlotResults(netlist, [x["reporter"] for x in parameters], inputs=inputs, smoothing=int(parameter["ep"]/2))
        print("\tSaving Plots...")
        HelpFunc.SavePlots(folder_train, figures, figurenames)
        print("\tSaving Plots -DONE-")
        print("\tSaving Parameters...")
        torch.save(parameters, folder_train+"parameters.pt", pickle_module=dill)
        print("\tSaving Parameters -DONE-")
        print("\tSaving Optimizers...")
        torch.save(optimizers, folder_train+"optimizers.pt", pickle_module=dill)
        print("\tSaving Optimizers -DONE-")
        print("\tSaving Schedulers...")
        torch.save(schedulers, folder_train+"schedulers.pt", pickle_module=dill)
        print("\tSaving Schedulers -DONE-")
        print("\tSaving Augmenters...")
        torch.save(augmenters, folder_train+"Augmenters.pt", pickle_module=dill)
        print("\tSaving Augmenters -DONE-")
        print("\tSaving Nets...")
        torch.save(netlist, folder_train+"nets.pt", pickle_module=dill)
        print("\tSaving Nets -DONE-")
    else:
        print("No nets to train found.")























