#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 11:11:31 2020

@author: at-lab
"""


import torch
import matplotlib.pyplot as plt
import numpy as np

global _FILENAME, _COLORS, _DEVICE
_FILENAME="Experiments.xls"
_COLORS=["royalblue", "maroon", "green", "black", "orangered", "gray", "teal", "purple", "goldenrod", "mediumslateblue"]
#_DEVICE="cuda" if torch.cuda.is_available() else "cpu"
#_DEVICE="cpu"


plt.rc('font',family='Times New Roman')
SMALL_SIZE=13
SMALL_MEDIUM=14
MEDIUM_SIZE=16
BIG_MEDIUM=18
BIG_SIZE=20
plt.rc('font', size=SMALL_MEDIUM)          # controls default text sizes
plt.rc('axes', titlesize=BIG_MEDIUM)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_MEDIUM)    # legend fontsize
plt.rc('figure', titlesize=BIG_SIZE)  # fontsize of the figure title



class DynReport():
    """how to use:
    -create object with column names
    -obj.GetHead() to print column names
    -obj.SetValues(list) append values to obj.VALUES
    -obj.Show() show last values
    -obj.NewLine() to create new line
        
    testobj=DynReport(["string", "float", "int", "torchfloat", "torchint"], precision=3)
    testobj.GetHead()
    for i in range(100):
        testobj.Update(["asd", torch.rand(1,1).item(), i, torch.rand((1,1), dtype=torch.float), torch.randint(-1,6, (1,1))])
        if i%10==0:
            testobj.NewLine()
        time.sleep(0.5)
    """
    def __init__(self, names, precision, average=10, show=25, line=250, header=5000, plotsize=4, autoshow=True, step=1):
        self.NAMES=names
        self.AVERAGE=average
        self.SHOW=show
        self.LINE=line
        self.HEADER=header
        self.AUTOSHOW=autoshow
        self.BATCH=0
        self.STEP=step
        
        if "Loss" in names:
            self.NAMES.append("avg. Loss")
        if "Acc" in names:
            self.NAMES.append("avg. Acc")
        self.LENS=[len(self.NAMES[idx])+2+precision for idx in range(len(self.NAMES))]
        self.VALS={}
        for name in names:
            self.VALS[name]=[]
        self.COLS=len(names)
        self.PRECISION=precision  
        self.PLOTSIZE=plotsize
        self.DUMPDICT={}
        
        
    def SetValues(self, vals, add=True):
        for col in range(self.COLS):
            name=self.NAMES[col]
            if name=="avg. Loss":
                self.VALS[name].append(torch.sum(torch.tensor(self.VALS["Loss"][-self.AVERAGE:]))/self.AVERAGE)
            elif name=="avg. Acc":
                self.VALS[name].append(torch.sum(torch.tensor(self.VALS["Acc"][-self.AVERAGE:]))/self.AVERAGE)
            else:
                self.VALS[name].append(vals[col])
                
        if self.AUTOSHOW:
            if self.BATCH==0:
                self.GetHead()
                self.Show()
                self.NewLine()
            else:
                if self.BATCH%self.SHOW==0:
                    self.Show()
                if self.BATCH%self.LINE==0:
                    self.NewLine()
                if self.BATCH%self.HEADER==0:
                    self.GetHead()
            if add:
                self.BATCH+=self.STEP            
    
    def Show(self):
        outstr="\r"
        args=[]
        for col in range(self.COLS):
            val=self.VALS[self.NAMES[col]][-1]  
            outstr+="{:s}"
            
            if type(val)==float:
                val=str(round(val, self.PRECISION))
            elif type(val)==torch.Tensor:
                val=str(round(val.item(), self.PRECISION))
            else:
                val=str(val)
            args.append(val+(self.LENS[col]-len(val))*" ")
        print(outstr.format(*args), end="")
        
    def GetHead(self):
        self.NewLine()
        string=""
        for col in range(self.COLS):
            name=self.NAMES[col]
            string+=name+(self.LENS[col]-len(name))*" "
        print(string)
        
    def NewLine(self):
        print("")      
        
    def PlotHistory(self, plots, smoothing=10):
        plotlist=[]
        for plotname in plots:
            values=self.VALS[plotname]
            fig=plt.figure(figsize=(3*self.PLOTSIZE,self.PLOTSIZE))
            
            ax=plt.subplot(1,1,1)
            plt.plot(MovingAverage(values, window=smoothing))
            
            plt.xlim((0, len(values)))
            plt.ylim((0, max(values)))
            ax.legend([plotname], bbox_to_anchor=(1,0.5), loc="center left")
            ax.set_title(plotname)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
            plotlist.append(fig)
            
        if len(plots)==1:
            return plotlist[0]
        else:
            return plotlist
        
    def PlotLoss(self, bounds=[-1, -1], avg=False, smoothing_mul=1):
        legendstr=["Loss"]
        if avg:
            legendstr.append("avg. Loss")
        
        if bounds==[-1,-1]:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,self.PLOTSIZE))
            fig.suptitle("Training Loss")
            
            ax=plt.subplot(1,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Loss"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(MovingAverage(self.VALS["avg. Loss"], window=smoothing_mul), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("original and averaged Loss")
            else:
                ax.set_title("original Loss")
            plt.xlim((0, len(self.VALS["Loss"])))
            plt.ylim((0, max(self.VALS["Loss"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        else:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,2*self.PLOTSIZE))
            fig.suptitle("Training Loss")
            
            ax=plt.subplot(2,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Loss"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(MovingAverage(self.VALS["avg. Loss"], window=smoothing_mul), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("zoomed original and averaged Loss")
            else:
                ax.set_title("zoomed original Loss")
            plt.xlim((0, len(self.VALS["Loss"])))
            plt.ylim((0, max(self.VALS["Loss"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            ax=plt.subplot(2,1,2)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Loss"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(MovingAverage(self.VALS["avg. Loss"], window=smoothing_mul), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("zoomed original and averaged Loss")
            plt.xlim((0, len(self.VALS["Loss"])))
            plt.ylim((bounds[0], bounds[1]))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")

            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        
        return fig
        
    def PlotAccuracy(self, bounds=[-1, -1], avg=False, smoothing_mul=1):
        legendstr=["Acc"]
        if avg:
            legendstr.append("avg. Acc")
        
        if bounds==[-1,-1]:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,self.PLOTSIZE))
            fig.suptitle("Testing Accuracies")
            
            ax=plt.subplot(1,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Acc"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(np.convolve(self.VALS["avg. Acc"], np.ones((smoothing_mul,))/smoothing_mul, mode='full'), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("original and averaged Accuracy")
            else:
                ax.set_title("original Accuracy")
            plt.xlim((0, len(self.VALS["Acc"])))
            plt.ylim((0, max(self.VALS["Acc"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        else:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,2*self.PLOTSIZE))
            fig.suptitle("Testing Accuracies")
            
            ax=plt.subplot(2,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Acc"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(np.convolve(self.VALS["avg. Acc"], np.ones((smoothing_mul,))/smoothing_mul, mode='full'), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("original and averaged Accuracy")
            else:
                ax.set_title("original Accuracy")
            
            plt.xlim((0, len(self.VALS["Acc"])))
            plt.ylim((0, max(self.VALS["Acc"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            ax=plt.subplot(2,1,2)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Acc"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(np.convolve(self.VALS["avg. Acc"], np.ones((smoothing_mul,))/smoothing_mul, mode='full'), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("zoomed original and averaged Accuracy")
            else:
                ax.set_title("zoomed Accuracy")
            plt.xlim((0, len(self.VALS["Acc"])))
            plt.ylim((bounds[0], bounds[1]))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        
        return fig
    
    
    
    
    
def ReporterFromString(string):
    step=int(string[:string.find(",")])
    string=string[string.find(",")+1:]

    avg=int(string[:string.find(",")])
    string=string[string.find(",")+1:]

    show=int(string[:string.find(",")])
    string=string[string.find(",")+1:]

    line=int(string[:string.find(",")])
    string=string[string.find(",")+1:]

    header=int(string)

    names=["Minibatch", "Acc", "Loss", "Test Acc", "Test Loss"]
    reporter=DynReport(names=names,
                                precision=3,
                                average=avg,
                                show=show,
                                line=line,
                                header=header,
                                autoshow=True,
                                step=step)
    return reporter


def MovingAverage(data, window=-1):
    cumsum = np.cumsum(np.insert(data, 0, 0)) 
    return (cumsum[window:] - cumsum[:-window]) / float(window)


def PlotResults(reporters, names, smoothing=5):
    figurelist=[]
    figurenames=[]
    LegendString=[]
    LegendStringT=[]

    alpha_low=0.4
    alpha_high=0.9

    for name in names:
        LegendString.append(name+" train")
        LegendString.append(name+" test")

    # Preprocess data

    xMax=0
    for reporter in reporters:
        if len(reporter.VALS["Minibatch"])==0:
            reporter.VALS["Minibatch"] = [0]*1000
            reporter.VALS["Acc"] = [0]*1000
            reporter.VALS["Loss"] = [0]*1000
            reporter.VALS["Test Acc"] = [0]*1000
            reporter.VALS["Test Loss"] = [0]*1000

        xMax=max(reporter.VALS["Minibatch"][-1], xMax)

    SmoothingMuls=[1,5]
    TrainLossList=[None]*len(reporters)
    TrainLossZoomList=[None]*len(reporters)
    TestLossList=[None]*len(reporters)
    TestLossZoomList=[None]*len(reporters)
    TrainAccList=[None]*len(reporters)
    TrainAccZoomList=[None]*len(reporters)
    TestAccList=[None]*len(reporters)
    TestAccZoomList=[None]*len(reporters)

    xList=[None]*len(reporters)
    xZoomList=[None]*len(reporters)

    yMaxLoss=0
    yMinLoss=float("Inf")
    yMaxLossZoomTrain=0
    yMinLossZoomTrain=float("Inf")
    yMaxLossZoomTest=0
    yMinLossZoomTest=float("Inf")

    yMaxAcc=0
    yMinAcc=float("Inf")
    yMaxAccZoomTrain=0
    yMinAccZoomTrain=float("Inf")
    yMaxAccZoomTest=0
    yMinAccZoomTest=float("Inf")

    for netnum in range(len(reporters)):
        TrainLossList[netnum]=MovingAverage(reporters[netnum].VALS["Loss"], window=SmoothingMuls[0]*smoothing)
        TrainLossZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Loss"], window=SmoothingMuls[1]*smoothing)
        TestLossList[netnum]=MovingAverage(reporters[netnum].VALS["Test Loss"], window=SmoothingMuls[0]*smoothing)
        TestLossZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Test Loss"], window=SmoothingMuls[1]*smoothing)

        TrainAccList[netnum]=MovingAverage(reporters[netnum].VALS["Acc"], window=SmoothingMuls[0]*smoothing)
        TrainAccZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Acc"], window=SmoothingMuls[1]*smoothing)
        TestAccList[netnum]=MovingAverage(reporters[netnum].VALS["Test Acc"], window=SmoothingMuls[0]*smoothing)
        TestAccZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Test Acc"], window=SmoothingMuls[1]*smoothing)

        if -SmoothingMuls[0]*smoothing+1<0:
            xList[netnum]=reporters[netnum].VALS["Minibatch"][:-SmoothingMuls[0]*smoothing+1]
        else:
            xList[netnum]=reporters[netnum].VALS["Minibatch"]
        if -SmoothingMuls[1]*smoothing+1<0:
            xZoomList[netnum]=reporters[netnum].VALS["Minibatch"][:-SmoothingMuls[1]*smoothing+1]
        else:
            xZoomList[netnum]=reporters[netnum].VALS["Minibatch"]

        yMaxLoss=max(yMaxLoss, max(torch.tensor(TrainLossList[netnum])[-1], torch.tensor(TestLossList[netnum])[-1]))
        yMinLoss=min(yMinLoss, min(torch.tensor(TrainLossList[netnum])[-1], torch.tensor(TestLossList[netnum])[-1]))
        yMaxLossZoomTrain=max(yMaxLossZoomTrain, torch.tensor(TrainLossZoomList[netnum])[-1])
        yMinLossZoomTrain=min(yMinLossZoomTrain, torch.tensor(TrainLossZoomList[netnum])[-1])
        yMaxLossZoomTest=max(yMaxLossZoomTest, torch.tensor(TestLossZoomList[netnum])[-1])
        yMinLossZoomTest=min(yMinLossZoomTest, torch.tensor(TestLossZoomList[netnum])[-1])

        yMaxAcc=max(yMaxAcc, max(torch.tensor(TrainAccList[netnum])[-1], torch.tensor(TestAccList[netnum])[-1]))
        yMinAcc=min(yMinAcc, min(torch.tensor(TrainAccList[netnum])[-1], torch.tensor(TestAccList[netnum])[-1]))
        yMaxAccZoomTrain=max(yMaxAccZoomTrain, torch.tensor(TrainAccZoomList[netnum])[-1])
        yMinAccZoomTrain=min(yMinAccZoomTrain, torch.tensor(TrainAccZoomList[netnum])[-1])
        yMaxAccZoomTest=max(yMaxAccZoomTest, torch.tensor(TestAccZoomList[netnum])[-1])
        yMinAccZoomTest=min(yMinAccZoomTest, torch.tensor(TestAccZoomList[netnum])[-1])

    d_val=1.5
    yLossMax=yMaxLoss*1.1
    difftrain=max(d_val*(yMaxLossZoomTrain-yMinLossZoomTrain)/2, 0.05)
    difftest=max(d_val*(yMaxLossZoomTest-yMinLossZoomTest)/2, 0.05)
    yLossZoomTrainMax=min((yMaxLossZoomTrain+yMinLossZoomTrain)/2+difftrain, 10)
    yLossZoomTrainMin=max((yMaxLossZoomTrain+yMinLossZoomTrain)/2-difftrain, -0.05)
    yLossZoomTestMax=min((yMaxLossZoomTest+yMinLossZoomTest)/2+difftest, 10)
    yLossZoomTestMin=max((yMaxLossZoomTest+yMinLossZoomTest)/2-difftest, -0.05)

    yAccMax=yMaxAcc*1.1
    difftrain=max(d_val*(yMaxAccZoomTrain-yMinAccZoomTrain)/2, 0.5)
    difftest=max(d_val*(yMaxAccZoomTest-yMinAccZoomTest)/2, 0.5)
    yAccZoomTrainMax=min((yMaxAccZoomTrain+yMinAccZoomTrain)/2+difftrain, 100.5)
    yAccZoomTrainMin=max((yMaxAccZoomTrain+yMinAccZoomTrain)/2-difftrain, -1)
    yAccZoomTestMax=min((yMaxAccZoomTest+yMinAccZoomTest)/2+difftest, 101)
    yAccZoomTestMin=max((yMaxAccZoomTest+yMinAccZoomTest)/2-difftest, -1)
    # Loss Plots
    fig=plt.figure(figsize=(12,12))
    fig.suptitle("Train and Testing Losses")

    ax=plt.subplot(3,1,1)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xList[netnum], TrainLossList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xList[netnum], TestLossList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("0 to %.2f"%(yLossMax))
    plt.xlim((0, xMax))
    plt.ylim((0, yLossMax))

    ax=plt.subplot(3,1,2)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yLossZoomTrainMin, yLossZoomTrainMax))
    plt.xlim((0, xMax))
    plt.ylim((yLossZoomTrainMin, yLossZoomTrainMax))

    ax=plt.subplot(3,1,3)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yLossZoomTestMin, yLossZoomTestMax))
    plt.xlim((0, xMax))
    plt.ylim((yLossZoomTestMin, yLossZoomTestMax))

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    figurelist.append(fig)
    figurenames.append("Losses")

    # Accuracy Plots
    fig=plt.figure(figsize=(12,12))
    fig.suptitle("Train and Testing Accuracies")

    ax=plt.subplot(3,1,1)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xList[netnum], TrainAccList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xList[netnum], TestAccList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("0% to 100% Accuracy")
    plt.xlim((0, xMax))
    plt.ylim((0, 105))

    ax=plt.subplot(3,1,2)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yAccZoomTrainMin, yAccZoomTrainMax))
    plt.xlim((0, xMax))
    plt.ylim((yAccZoomTrainMin, yAccZoomTrainMax))

    ax=plt.subplot(3,1,3)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yAccZoomTestMin, yAccZoomTestMax))
    plt.xlim((0, xMax))
    plt.ylim((yAccZoomTestMin, yAccZoomTestMax))

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    figurelist.append(fig)
    figurenames.append("Accuracies")


    # Time Plots
#    TrainTimes=[]
#    for reporter in reporters:
#        TrainTimes.append(reporter.DUMPDICT["TrainTime"])
#    TrainTimesSorted=sorted(TrainTimes)
#    NamesSorted=[x for _, x in sorted(zip(TrainTimes, names))]
#
#    for netnum in range(len(reporters)):
#        LegendStringT.append(NamesSorted[netnum])
#
#    fig=plt.figure(figsize=(12,8))
#    plt.grid(b=True, which="both", axis="both")
#
#    for netnum in range(len(names)):
#        plt.bar(LegendStringT[netnum], TrainTimesSorted[netnum], color=_COLORS[netnum], edgecolor=_COLORS[netnum])
#    fig.suptitle("Training times\n sec / Epoch")
#
#    fig.tight_layout(rect=[0, 0.03, 1, 0.9])
#
#    figurelist.append(fig)
#    figurenames.append("TrainTimes")


    # Custom Kernel Plots

    return figurelist, figurenames

def SavePlots(path, figures, names, dpi=100):
    for idx in range(len(figures)):
        figures[idx].savefig(path+names[idx]+".png", dpi=dpi)
        
        
'''

#reporter	
#parameters for the reporter	a,b,c,d,e,f,g	
#a: Step (int, Save Numbers every a batches) 
#b: Avg (int, Smoothening window for average Display) 
#c: Show (int, Show new Values every c Batches) 
#d: Line (int, Show new Line every d Batches) 
#e: Header (int, Show new Header every e Batches)	
#5,20,25,250,5000	b,c,d,e should be divisible by a

string = '5,20,25,250,5000'
reporter = ReporterFromString(string)

reporter.SetValues([0, 60, 0.3, 0.3, 3])


figures, figurenames = PlotResults(reporter, "my_network", smoothing=20)
SavePlots(folder_train, figures, figurenames)



    batch=0

    for ep in range(epmax):
        for _, (data, label) in enumerate(trainer):
            data=data.to(_DEVICE)
            label=label.to(_DEVICE)
            optimizer.zero_grad()
            label=label.long()
            outs=net(data)

            loss=criterion(outs, label)+norm(net)
            loss.backward()
            optimizer.step()

            train_acc=100.0*float(torch.sum(torch.argmax(outs, dim=1)==label))/data.shape[0]

            if batch%testevery==0:
                lasttestloss, lasttestacc=TestNet(net, tester, criterion)

            if batch%reporter.STEP==0:
                reporter.SetValues([batch, train_acc, loss.item(), lasttestloss, lasttestacc])
            batch+=1
        scheduler.step()
        lasttestloss, lasttestacc=TestNet(net, tester, criterion)
    print("\n------ Last Values:")
    reporter.Show()
    print("\n------")
    net.WriteTimeEnd(retrain=True)
    net.WriteResults(reporter)

'''

