import torch
import numpy as np
import math
import time


class LogicLayerAN(torch.nn.Module):
    
    def __init__(self, in_features, out_features):
        super(LogicLayerAN, self).__init__()
        self.IN_FEATURES=in_features
        self.OUT_FEATURES=out_features
        
        self.register_parameter("A",torch.nn.Parameter((torch.rand(1, in_features, out_features, dtype=torch.double)-0.5)*1))
        self.register_parameter("O",torch.nn.Parameter((torch.rand(1, out_features, dtype=torch.double)-0.5)*1))
        
        self.A_INIT=self.A*1.0
        self.O_INIT=self.O*1.0
        self.EPS=0.0001
            
    def __repr__(self):
        return "LogicLayerAN(Ins: %i, Outs: %i)"%(self.IN_FEATURES, self.OUT_FEATURES)
            
    def forward(self, ins):  
        inputs=ins.double()
        batchsize=inputs.shape[0]
        inputs=inputs.view(batchsize, inputs.shape[1], 1).repeat(1, 1, self.OUT_FEATURES)
        ones_out=torch.ones(batchsize, self.OUT_FEATURES, dtype=torch.double).to(ins.device)

        # preprocessing
        aa=torch.sigmoid(self.A.repeat(batchsize, 1, 1))
        y_o=torch.sigmoid(self.O.repeat(batchsize, 1))
        
        # and calculation
        a_star_and=torch.exp(torch.bmm(torch.log(inputs+self.EPS).permute(0,2,1), aa))
        and_out=torch.zeros(batchsize, self.OUT_FEATURES, dtype=torch.double).to(ins.device)
        
        for batch in range(batchsize):
            and_out[batch]=torch.diag(a_star_and[batch]).view(1,-1)
            
        # output negation
        outs=(ones_out-and_out)*y_o + and_out*(ones_out-y_o)
        outs=torch.clamp(outs, self.EPS, 1-self.EPS)
        return outs  


##########################################
##########################################
##########################################
        
    
class LogicLayerNAN(torch.nn.Module):
    
    def __init__(self, in_features, out_features):
        super(LogicLayerNAN, self).__init__()
        self.IN_FEATURES=in_features
        self.OUT_FEATURES=out_features
        
        self.register_parameter("I",torch.nn.Parameter((torch.rand(1, in_features, out_features, dtype=torch.double)-0.5)*1))
        self.register_parameter("A",torch.nn.Parameter((torch.rand(1, in_features, out_features, dtype=torch.double)-0.5)*1))
        self.register_parameter("O",torch.nn.Parameter((torch.rand(1, out_features, dtype=torch.double)-0.5)*1))
        
        self.I_INIT=self.I*1.0
        self.A_INIT=self.A*1.0
        self.O_INIT=self.O*1.0
        self.EPS=0.0001
            
    def __repr__(self):
        return "LogicLayerNAN(Ins: %i, Outs: %i)"%(self.IN_FEATURES, self.OUT_FEATURES)
            
    def forward(self, ins):  
        inputs=ins.double()
        batchsize=inputs.shape[0]
        inputs=inputs.view(batchsize, inputs.shape[1], 1).repeat(1, 1, self.OUT_FEATURES)
        ones_in=torch.ones_like(inputs, dtype=torch.double).to(ins.device)
        ones_out=torch.ones(batchsize, self.OUT_FEATURES, dtype=torch.double).to(ins.device)
       
        # preprocessing
        y_i=torch.sigmoid(self.I.repeat(batchsize, 1, 1))
        aa=torch.sigmoid(self.A.repeat(batchsize, 1, 1))
        y_o=torch.sigmoid(self.O.repeat(batchsize, 1))
        
        # input negation, inputs for each rule
        x_hat=(ones_in-inputs)*y_i + inputs*(ones_in-y_i)
        
        # and calculation
        a_star_and=torch.exp(torch.bmm(torch.log(x_hat+self.EPS).permute(0,2,1), aa))
        and_out=torch.zeros(batchsize, self.OUT_FEATURES, dtype=torch.double).to(ins.device)
        
        for batch in range(batchsize):
            and_out[batch]=torch.diag(a_star_and[batch]).view(1,-1)
            
        # output negation
        outs=(ones_out-and_out)*y_o + and_out*(ones_out-y_o)
        outs=torch.clamp(outs, self.EPS, 1-self.EPS)
        return outs  


##########################################
##########################################
##########################################
        
    
class LogicLayerAO(torch.nn.Module):
    
    def __init__(self, in_features, out_features):
        super(LogicLayerAO, self).__init__()
        self.IN_FEATURES=in_features
        self.OUT_FEATURES=out_features
        
        self.register_parameter("I",torch.nn.Parameter((torch.rand(1, in_features, out_features)-0.5)*1))
        self.register_parameter("A",torch.nn.Parameter((torch.rand(1, in_features, out_features)-0.5)*1))
        self.register_parameter("O",torch.nn.Parameter((torch.rand(1, out_features)-0.5)*1))
        
        self.I_INIT=self.I*1.0
        self.A_INIT=self.A*1.0
        self.O_INIT=self.O*1.0
        self.EPS=0.0001
            
    def __repr__(self):
        return "LogicLayerAO(Ins: %i, Outs: %i)"%(self.IN_FEATURES, self.OUT_FEATURES)
            
    def forward(self, ins):  
        inputs=ins
        batchsize=inputs.shape[0]
        inputs=inputs.view(batchsize, inputs.shape[1], 1).repeat(1, 1, self.OUT_FEATURES)
        ones_in=torch.ones_like(inputs).to(ins.device)
        
        # preprocessing
        y_i=torch.sigmoid(self.I.repeat(batchsize, 1, 1))
        aa=torch.sigmoid(self.A.repeat(batchsize, 1, 1))
        y_o=torch.sigmoid(self.O.repeat(batchsize, 1))
        
        # input negation, inputs for each rule
        x_hat=(ones_in-inputs)*y_i + inputs*(ones_in-y_i)
        
        # and calculation
        a_star_and=torch.exp(torch.bmm(torch.log(x_hat+self.EPS).permute(0,2,1), aa))
        a_star_or=x_hat*aa
        and_out=torch.zeros(batchsize, self.OUT_FEATURES).to(ins.device)
        or_out=torch.zeros(batchsize, self.OUT_FEATURES).to(ins.device)
        
        for batch in range(batchsize):
            and_out[batch]=torch.diag(a_star_and[batch]).view(1,-1)
            or_out[batch]=self.Orfunc(a_star_or[batch])
            
        # output negation
        outs=and_out*y_o + or_out*(1-y_o)
        outs=torch.clamp(outs, self.EPS, 1-self.EPS)
        return outs  

    def Orfunc(self, ins):
        return torch.prod(1-ins[1:,:], dim=0)*(ins-1)[0,:]+1


##########################################
##########################################
##########################################
        
    
class GenDilation(torch.nn.Module):
    def __init__(self, in_channels, out_channels, field_size, kernel_size=0, stride=1, padding=0, coeff_exp=1, coeff_max=-1, coeff_lin=0.1, rows=True, cols=True, alls=True, range_init=1):
        super(GenDilation, self).__init__()
        
        #parameters
        self.STRIDE=stride
        self.PADDING=padding
        self.C_EXP=coeff_exp
        self.C_LIN=coeff_lin
        self.C_MAX=coeff_max
        self.FIELDSIZE=field_size
        self.INCHANNELS=in_channels
        self.OUTCHANNELS=out_channels
        self.ROWS=rows
        self.COLS=cols
        self.ALLS=alls
        
        if kernel_size==0:
            self.KERNELSIZE=field_size
        else:
            self.KERNELSIZE=kernel_size
        
        mean=-math.log(1/(kernel_size**2/field_size**2)-1)
        minval=mean-range_init
        maxval=mean+range_init        
        
        #weight
        self.register_parameter("weight",torch.nn.Parameter(-0.2+torch.rand(field_size*field_size*out_channels*in_channels,1).view(out_channels, in_channels, field_size, field_size)*0.4))
        
        #masking
        self.register_parameter("mask",torch.nn.Parameter(minval+torch.rand(field_size*field_size*out_channels*in_channels,1).view(out_channels, in_channels, field_size, field_size)*(maxval-minval)))
        
        #bias
        self.register_parameter("bias",torch.nn.Parameter(-0.2+torch.rand(out_channels,1).view(out_channels)*0.4))
        
            
    def __repr__(self):
        return "CustomConv2d(Kernel: %i, Field: %i, in_channels: %i, out_channels: %i, stride: %i, padding: %i, coeffs:(%f, %f))"%(self.KERNELSIZE, self.FIELDSIZE, self.INCHANNELS, self.OUTCHANNELS, self.STRIDE, self.PADDING, self.C_EXP, self.C_LIN)
    
    def forward(self, x):
        return cconv2d(x, self.weight, self.mask, self.bias, self.Constraints, self.STRIDE, self.PADDING).to(x.device)
    
    def Barrier(self, x, entries):        
        # barr=torch.exp(self.C_EXP*(x-entries))-self.C_LIN*(x-entries)-1
        barr=torch.exp(self.C_EXP*(x-entries))*self.C_LIN*(x-entries)
        return barr
    
    def Constraints(self):
        Mask=torch.sigmoid(self.mask)
        BarrierMatrix=torch.zeros_like(Mask)
        # calculate barrier values
        if self.ROWS:
            Rows=torch.sum(Mask, dim=3, keepdim=True)
            RowsB=self.Barrier(Rows, self.KERNELSIZE)
            RowsB=(RowsB>self.C_MAX).float()*RowsB + (RowsB<=self.C_MAX).float()*self.C_MAX
            BarrierMatrix+=RowsB
        
        # calculate barrier values
        if self.COLS:
            Cols=torch.sum(Mask, dim=2, keepdim=True)
            ColsB=self.Barrier(Cols, self.KERNELSIZE)
            ColsB=(ColsB>self.C_MAX).float()*ColsB + (ColsB<=self.C_MAX).float()*self.C_MAX
            BarrierMatrix+=ColsB
            
        # calculate barrier values
        if self.ALLS:
            Alls=torch.sum(Mask, dim=(2,3), keepdim=True)
            AllsB=self.Barrier(Alls, self.KERNELSIZE**2)
            AllsB=(AllsB>self.C_MAX).float()*AllsB + (AllsB<=self.C_MAX).float()*self.C_MAX
            BarrierMatrix+=AllsB        
        return BarrierMatrix
    
class CustomConv(torch.autograd.Function):
    @staticmethod    
    def forward(ctx, x, w, m, b, constraintfunc, stride=1, padding=0):
        ctx.stride=stride
        ctx.padding=padding
        ctx.save_for_backward(x, w, m, b)
        ctx.func=constraintfunc
        return torch.nn.functional.conv2d(x, weight=torch.sigmoid(m)*w, bias=b, stride=ctx.stride, padding=ctx.padding)
    
    @staticmethod    
    def backward(ctx, grad_output):
        x, w, m, b=ctx.saved_tensors
        
        m_sig=torch.sigmoid(m)
        grad_weight=torch.nn.grad.conv2d_weight(x, w.shape, grad_output, stride=ctx.stride, padding=ctx.padding)
        grad_x=torch.nn.grad.conv2d_input(x.shape, m_sig*w, grad_output, stride=ctx.stride, padding=ctx.padding)
        grad_w=grad_weight*m_sig
        grad_m=(grad_weight+ctx.func())*m_sig*(1-m_sig)
        grad_b=torch.mean(grad_output.sum(0), dim=(1,2))
        
        return grad_x, grad_w, grad_m, grad_b, None, None, None    
    
class ForSigBackLinClass(torch.autograd.Function):
    @staticmethod    
    def forward(ctx, x, slope=0.5, offset=0):
        ctx.save_for_backward(slope, offset)
        return torch.sigmoid(x)
    
    @staticmethod    
    def backward(ctx, grad_output):
        slope, offset=ctx.saved_tensors
        return grad_output*slope + offset
       
cconv2d=CustomConv.apply
ForwSig_BackwLin=ForSigBackLinClass.apply  


##########################################
##########################################
##########################################
        
    
class DummyNet(torch.nn.Module):
    def __init__(self, ExcelCol):
        super(DummyNet, self).__init__()
        self.SEQUENCE=torch.nn.Sequential()
        self.COL=ExcelCol
        self.SHEET=0
        self.NAME=0
        self.BOOK=0
        self.FOLDER=0
        self.KEEPTRAIN=0
        self.NROWS=0
        self.MASKS=[]
        self.LLI=[]
        self.LLA=[]
        self.LLO=[]
        
    def forward(self, x):
        return self.SEQUENCE(x)

    def AddLayer(self, layertype, layer):
        self.SEQUENCE.add_module(str(len(self.SEQUENCE)+1)+"_"+layertype, layer)
    
    def Start(self):
        self.WriteTimeStart()
        for layer in self.SEQUENCE:
            if isinstance(layer, CustomConv):
                self.MASKS.append(layer.mask*1.0)  
            if isinstance(layer, LLAN):
                self.LLI.append([])  
                self.LLA.append(layer.A*1.0)  
                self.LLO.append(layer.O*1.0)  
            if isinstance(layer, LLNAN):
                self.LLI.append(layer.I*1.0)  
                self.LLA.append(layer.A*1.0)  
                self.LLO.append(layer.O*1.0)  
            if isinstance(layer, LLAO):
                self.LLI.append(layer.I*1.0)  
                self.LLA.append(layer.A*1.0)  
                self.LLO.append(layer.O*1.0)          
        
        
    def WriteTimeStart(self):
        self.SHEET.write(2, self.COL, time.strftime("%y_%m_%d__%H_%M_%S"))
        
    def WriteTimeEnd(self):
        self.SHEET.write(3, self.COL, time.strftime("%y_%m_%d__%H_%M_%S"))
        if not self.KEEPTRAIN:
            self.SHEET.write(4, self.COL, 0)
            
    def WriteFolder(self):
        XlWrite(self.SHEET, 23, self.COL, "file:///"+self.FOLDER, "linux", link=True)
        XlWrite(self.SHEET, 24, self.COL, "Z:\\400__Server\\Reimann\\01_Dilation\\00_Train\\"+time_str+"\\", "windows", link=True)
    
    def WriteResults(self, reporter):
        lasttrainloss=float(reporter.VALS["avg. Loss"][-1])
        lasttesloss=float(reporter.VALS["Test Loss"][-1])
        
        lasttrainacc=float(reporter.VALS["avg. Acc"][-1])
        lastestacc=float(reporter.VALS["Test Acc"][-1])
        
        XlWrite(self.SHEET, 19, self.COL, lastestacc, lastestacc)
        XlWrite(self.SHEET, 20, self.COL, lasttesloss, lasttesloss)
        XlWrite(self.SHEET, 21, self.COL, lasttrainacc, lasttrainacc)
        XlWrite(self.SHEET, 22, self.COL, lasttrainloss, lasttrainloss)
    


















