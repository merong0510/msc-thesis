import torch


class ForwardSigmoid_BackwardLinFunc(torch.autograd.Function):
    @staticmethod    
    def forward(ctx, x, slope=0.5, offset=0):
        ctx.slope=slope
        ctx.offset=offset
        return torch.sigmoid(x)
    
    @staticmethod    
    def backward(ctx, grad_output):
        return grad_output*ctx.slope + ctx.offset,  grad_output*torch.ones_like(ctx.slope),  grad_output*torch.zeros_like(ctx.offset)
      

class ForwardHeavy_BackwardLinFunc(torch.autograd.Function):
    @staticmethod    
    def forward(ctx, x, slope=0.5, offset=0):
        ctx.slope=slope
        ctx.offset=offset
        return HeavySide(x)
    
    @staticmethod    
    def backward(ctx, grad_output):
        return grad_output*ctx.slope + ctx.offset, grad_output*torch.zeros_like(ctx.slope), grad_output*torch.zeros_like(ctx.offset)
      
        
def HeavySide(ins):
    if ins>0:
        return torch.ones_like(ins)
    else:
        return torch.zeros_like(ins)
    
#class SwishClass(torch.autograd.Function):
#    @staticmethod    
#    def forward(ctx, x, beta):
#        ctx.beta=beta
#        f=x*torch.sigmoid(beta*x)
#        ctx.save_for_backward(f, x)
#        return f
#    
#    @staticmethod    
#    def backward(ctx, grad_output):
#        f, x =ctx.saved_tensors
#        return grad_output*(ctx.beta*f+torch.sigmoid(ctx.beta*x)*(1-ctx.beta*f)), grad_output*(f*(x-f))
#
#SwishFunc=SwishClass.apply

class SwishModule(torch.nn.Module):
    def __init__(self, beta, betatrain=False):
        super(SwishModule, self).__init__()
        if betatrain:
            self.register_parameter("BETA",torch.nn.Parameter(torch.tensor(beta)))
        else:
            self.BETA=torch.tensor(beta)

    def forward(self, x):
        return x*torch.sigmoid(x*self.BETA)  
    
    def __repr__(self):
        return("Swish Function (beta= %.4f)"%(self.BETA))
                
      