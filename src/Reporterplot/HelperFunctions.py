import math as m
import random
import torch
import torch.utils.data
import torchvision
import pickle as pkl
import numpy as np
import time
import os
import matplotlib.pyplot as plt
from Classes import LogicLayerAN as LLAN
from Classes import LogicLayerNAN as LLNAN
from Classes import LogicLayerAO as LLAO
from Classes import GenDilation as cConv2d


global _COLORS, _DEVICE
_COLORS=["royalblue", "maroon", "green", "black", "orangered", "gray", "teal", "purple", "goldenrod", "mediumslateblue"]
_DEVICE="cuda" if torch.cuda.is_available() else "cpu"

plt.rc('font',family='Times New Roman')
SMALL_SIZE=13
SMALL_MEDIUM=14
MEDIUM_SIZE=16
BIG_MEDIUM=18
BIG_SIZE=20
plt.rc('font', size=SMALL_MEDIUM)          # controls default text sizes
plt.rc('axes', titlesize=BIG_MEDIUM)     # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_MEDIUM)    # legend fontsize
plt.rc('figure', titlesize=BIG_SIZE)  # fontsize of the figure title

################################################
################################################
################################################

class TimeLine:
    VALUES={}
    NAMES=[]
    
    def __init__(self, names):
        self.VALUES={}
        self.NAMES=names
        for name in names:
            self.VALUES[name]=[]

    def Step(self, name, value):
        if type(value)==torch.Tensor:
            self.VALUES[name].append(float(value.numpy()))
        else:
            self.VALUES[name].append(value)
        
    def ClearVal(self, name):
        self.VALUES[name]=[]
        
    def PrintAll(self, OnlyShapes=False):
        for name in self.NAMES:
            print("%s:\n"%name)
            for val in self.VALUES[name]:
                if type(val)==int:
                    print("\t %i\n"%val)
                if type(val)==float:
                    print("\t %f\n"%val)
                if type(val)==str:
                    print("\t %s\n"%val)
            
################################################
################################################
################################################    
                    
def LoadMnist(minibatch=60, normalize=True, sciebo=False):
    if sciebo:
        path="/home/at-lab/ownCloud/02_NAS/00_Datasets/01_MNIST"
    else:
        path="/media/NAS/Reimann/00_Datasets/01_MNIST"
    if normalize:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor(),
                                                   torchvision.transforms.Normalize(
                                                           (0.1307,), (0.3081,))
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor(),
                                                   torchvision.transforms.Normalize(
                                                           (0.1307,), (0.3081,))
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    else:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.MNIST(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    
    return list(enumerate(train_loader)), list(enumerate(test_loader)), train_loader, test_loader
################################################
################################################
################################################    
                    
def LoadCifar10Gray(minibatch=50, normalize=True, grayscale=True, sciebo=False):
    if sciebo:
        path="/home/at-lab/ownCloud/02_NAS/00_Datasets/01_MNIST"
    else:
        path="/media/NAS/Reimann/00_Datasets/01_MNIST"
    if normalize:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.Grayscale(num_output_channels=1),
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.Grayscale(num_output_channels=1),
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    else:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.Grayscale(num_output_channels=1),
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.Grayscale(num_output_channels=1),
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    return list(enumerate(train_loader)), list(enumerate(test_loader)), train_loader, test_loader
            
################################################
################################################
################################################    
                    
def LoadCifar10RGB(minibatch=50, normalize=True, grayscale=True, sciebo=False):  
    if sciebo:
        path="/home/at-lab/ownCloud/02_NAS/00_Datasets/02_CIFAR10"
    else:
        path="/media/NAS/Reimann/00_Datasets/02_CIFAR10"
    if normalize:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor(),
                                                   torchvision.transforms.Normalize(
                                                           (0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor(),
                                                   torchvision.transforms.Normalize(
                                                           (0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    else:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR10(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    return list(enumerate(train_loader)), list(enumerate(test_loader)), train_loader, test_loader  
            
################################################
################################################
################################################     
                    
def LoadCifar100RGB(minibatch=50, normalize=True, grayscale=True, sciebo=False):  
    if sciebo:
        path="/home/at-lab/ownCloud/02_NAS/00_Datasets/04_CIFAR100"
    else:
        path="/media/NAS/Reimann/00_Datasets/04_CIFAR100"
    if normalize:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR100(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor(),
                                                   torchvision.transforms.Normalize(
                                                           (0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR100(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor(),
                                                   torchvision.transforms.Normalize(
                                                           (0.4914, 0.4822, 0.4465), (0.247, 0.243, 0.261))
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    else:
        train_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR100(root=path, train=True, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
        
        test_loader = torch.utils.data.DataLoader(
                torchvision.datasets.CIFAR100(root=path, train=False, download=True,
                                           transform=torchvision.transforms.Compose([
                                                   torchvision.transforms.ToTensor()
                                                   ])),
                                                   batch_size=minibatch, shuffle=True, pin_memory=True)
    return list(enumerate(train_loader)), list(enumerate(test_loader)), train_loader, test_loader  
            
################################################
################################################
################################################    
    
                   
class TEastmanDataset(torch.utils.data.Dataset):   
    def __init__(self, folder="/home/at-lab/ownCloud/99_Datasets/03_TE/TE_process/", train=True, cases=range(22), normalize=False, onehot=False):   
        self.DATA=torch.zeros(0, 52)
        self.LABEL=torch.zeros(0)
        if train:
            ending=".dat"
        else:
            ending="_te.dat"
        
        for error_mode in cases:   
            if error_mode<10:
                name=folder+"d0"+str(error_mode)+ending
            else:
                name=folder+"d"+str(error_mode)+ending
                
            data=torch.tensor(np.genfromtxt(name)).float()
            
            if error_mode==0 and train:
                data=data.permute(1,0)
                
            labels=torch.tensor(error_mode).view(1).float().repeat(data.shape[0])
            
            self.DATA=torch.cat((self.DATA, data), dim=0)
            self.LABEL=torch.cat((self.LABEL, labels), dim=0)
            
        if normalize:
            self.DATA = (self.DATA - self.DATA.mean(dim=0))/self.DATA.std(dim=0)
            
        if onehot:
            helper=torch.zeros(self.LABEL.shape[0], 22)
            
            for batch in range(self.LABEL.shape[0]):
                helper[batch, int(self.LABEL[batch])]=1
            self.LABEL=helper
        self.LABEL=self.LABEL.long()
        
    def __len__(self):
        return self.DATA.shape[0]
    
    def __getitem__(self, idx):
        return self.DATA[idx], self.LABEL[idx]

################################################
################################################
################################################
    
    
class ParameterFile:
    FOLDER=""
    FILE=""
    RIGHTS=""
    LENGTHS=[]
    FID=[]
    
    def __init__(self, foldername, lengths=30):
        self.FOLDER=foldername
        self.FILE="ParameterList.txt"
        self.LENGTHS=lengths
        
    def Create(self):
        if not os.path.exists(self.FOLDER):
            os.makedirs(self.FOLDER)
        self.FID=open(self.FOLDER + self.FILE, "w")
            
    def Write(self, txt):
        self.FID=open(self.FOLDER + self.FILE, "a")
        self.FID.write(txt+"\n")
        self.FID.close()
        
    def WriteTab(self, name, val):
        self.FID=open(self.FOLDER + self.FILE, "a")
        self.FID.write(name)
        
        if type(val)==list:
            c=0
            for i in val:
                if c==0:
                    self.FID.write(" "*(self.LENGTHS-len(name)))
                else:
                    self.FID.write(" "*self.LENGTHS)
                    
                if type(i)==int:
                    self.FID.write("%i\n"%i)
                elif type(i)==float:
                    self.FID.write("%f\n"%i)
                else:
                    self.FID.write("%s\n"%i)
                c+=1
        elif type(val)==int:
            self.FID.write(" "*(self.LENGTHS-len(name))+"%i\n"%val)
        elif type(val)==float:
            self.FID.write(" "*(self.LENGTHS-len(name))+"%f\n"%val)
        else:
            self.FID.write(" "*(self.LENGTHS-len(name))+"%s\n"%val)
        self.FID.write("\n")
        self.FID.close()
        
    def DashSigns(self):
        self.FID=open(self.FOLDER + self.FILE, "a")
        self.FID.write("---------------------------\n")
        self.FID.close()
        
    def EqualSigns(self):
        self.FID=open(self.FOLDER + self.FILE, "a")
        self.FID.write("===========================\n")
        self.FID.close()
        
        
            
################################################
################################################
################################################
    
def OneHotVector(loc, maxlen=10):
    if type(loc)==torch.Tensor:
        batchsize=loc.shape[0]
        retval=torch.zeros((batchsize, maxlen), dtype=torch.long)
        for i in range(batchsize):
            retval[i,loc[i]]=1
    else:
        retval=torch.zeros((1,maxlen), dtype=torch.long)
        retval[0,loc]=1
        
    return retval
            
################################################
################################################
################################################
    
def MovingAverage(data, window=-1):
    cumsum = np.cumsum(np.insert(data, 0, 0)) 
    return (cumsum[window:] - cumsum[:-window]) / float(window)

################################################
################################################
################################################
    
def GetRandomSample(DataList):
    batch, (data, label) = random.choice(DataList)
    return data, label
            
################################################
################################################
################################################
    
def RandTensorRange(shape, minval, maxval):
    x=minval+torch.rand(shape)*(maxval-minval)
    return x
            
################################################
################################################
################################################
    

class DynReport():
    """how to use:
    -create object with column names
    -obj.GetHead() to print column names
    -obj.SetValues(list) append values to obj.VALUES
    -obj.Show() show last values
    -obj.NewLine() to create new line
        
    testobj=DynReport(["string", "float", "int", "torchfloat", "torchint"], precision=3)
    testobj.GetHead()
    for i in range(100):
        testobj.Update(["asd", torch.rand(1,1).item(), i, torch.rand((1,1), dtype=torch.float), torch.randint(-1,6, (1,1))])
        if i%10==0:
            testobj.NewLine()
        time.sleep(0.5)
    """
    def __init__(self, names, precision, average=10, show=25, line=250, header=5000, plotsize=4, autoshow=True, step=1):
        self.NAMES=names
        self.AVERAGE=average
        self.SHOW=show
        self.LINE=line
        self.HEADER=header
        self.AUTOSHOW=autoshow
        self.BATCH=0
        self.STEP=step
        
        if "Loss" in names:
            self.NAMES.append("avg. Loss")
        if "Acc" in names:
            self.NAMES.append("avg. Acc")
        self.LENS=[len(self.NAMES[idx])+2+precision for idx in range(len(self.NAMES))]
        self.VALS={}
        for name in names:
            self.VALS[name]=[]
        self.COLS=len(names)
        self.PRECISION=precision  
        self.PLOTSIZE=plotsize
        self.DUMPDICT={}
        
        
    def SetValues(self, vals, add=True):
        for col in range(self.COLS):
            name=self.NAMES[col]
            if name=="avg. Loss":
                self.VALS[name].append(torch.sum(torch.tensor(self.VALS["Loss"][-self.AVERAGE:]))/self.AVERAGE)
            elif name=="avg. Acc":
                self.VALS[name].append(torch.sum(torch.tensor(self.VALS["Acc"][-self.AVERAGE:]))/self.AVERAGE)
            else:
                self.VALS[name].append(vals[col])
                
        if self.AUTOSHOW:
            if self.BATCH==0:
                self.GetHead()
                self.Show()
                self.NewLine()
            else:
                if self.BATCH%self.SHOW==0:
                    self.Show()
                if self.BATCH%self.LINE==0:
                    self.NewLine()
                if self.BATCH%self.HEADER==0:
                    self.GetHead()
            if add:
                self.BATCH+=self.STEP            
    
    def Show(self):
        outstr="\r"
        args=[]
        for col in range(self.COLS):
            val=self.VALS[self.NAMES[col]][-1]  
            outstr+="{:s}"
            
            if type(val)==float:
                val=str(round(val, self.PRECISION))
            elif type(val)==torch.Tensor:
                val=str(round(val.item(), self.PRECISION))
            else:
                val=str(val)
            args.append(val+(self.LENS[col]-len(val))*" ")
        print(outstr.format(*args), end="")
        
    def GetHead(self):
        self.NewLine()
        string=""
        for col in range(self.COLS):
            name=self.NAMES[col]
            string+=name+(self.LENS[col]-len(name))*" "
        print(string)
        
    def NewLine(self):
        print("")      
        
    def PlotHistory(self, plots, smoothing=10):
        plotlist=[]
        for plotname in plots:
            values=self.VALS[plotname]
            fig=plt.figure(figsize=(3*self.PLOTSIZE,self.PLOTSIZE))
            
            ax=plt.subplot(1,1,1)
            plt.plot(MovingAverage(values, window=smoothing))
            
            plt.xlim((0, len(values)))
            plt.ylim((0, max(values)))
            ax.legend([plotname], bbox_to_anchor=(1,0.5), loc="center left")
            ax.set_title(plotname)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
            plotlist.append(fig)
            
        if len(plots)==1:
            return plotlist[0]
        else:
            return plotlist
        
    def PlotLoss(self, bounds=[-1, -1], avg=False, smoothing_mul=1):
        legendstr=["Loss"]
        if avg:
            legendstr.append("avg. Loss")
        
        if bounds==[-1,-1]:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,self.PLOTSIZE))
            fig.suptitle("Training Loss")
            
            ax=plt.subplot(1,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Loss"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(MovingAverage(self.VALS["avg. Loss"], window=smoothing_mul), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("original and averaged Loss")
            else:
                ax.set_title("original Loss")
            plt.xlim((0, len(self.VALS["Loss"])))
            plt.ylim((0, max(self.VALS["Loss"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        else:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,2*self.PLOTSIZE))
            fig.suptitle("Training Loss")
            
            ax=plt.subplot(2,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Loss"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(MovingAverage(self.VALS["avg. Loss"], window=smoothing_mul), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("zoomed original and averaged Loss")
            else:
                ax.set_title("zoomed original Loss")
            plt.xlim((0, len(self.VALS["Loss"])))
            plt.ylim((0, max(self.VALS["Loss"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            ax=plt.subplot(2,1,2)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Loss"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(MovingAverage(self.VALS["avg. Loss"], window=smoothing_mul), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("zoomed original and averaged Loss")
            plt.xlim((0, len(self.VALS["Loss"])))
            plt.ylim((bounds[0], bounds[1]))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")

            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        
        return fig
        
    def PlotAccuracy(self, bounds=[-1, -1], avg=False, smoothing_mul=1):
        legendstr=["Acc"]
        if avg:
            legendstr.append("avg. Acc")
        
        if bounds==[-1,-1]:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,self.PLOTSIZE))
            fig.suptitle("Testing Accuracies")
            
            ax=plt.subplot(1,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Acc"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(np.convolve(self.VALS["avg. Acc"], np.ones((smoothing_mul,))/smoothing_mul, mode='full'), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("original and averaged Accuracy")
            else:
                ax.set_title("original Accuracy")
            plt.xlim((0, len(self.VALS["Acc"])))
            plt.ylim((0, max(self.VALS["Acc"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        else:
            fig=plt.figure(figsize=(3*self.PLOTSIZE,2*self.PLOTSIZE))
            fig.suptitle("Testing Accuracies")
            
            ax=plt.subplot(2,1,1)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Acc"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(np.convolve(self.VALS["avg. Acc"], np.ones((smoothing_mul,))/smoothing_mul, mode='full'), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("original and averaged Accuracy")
            else:
                ax.set_title("original Accuracy")
            
            plt.xlim((0, len(self.VALS["Acc"])))
            plt.ylim((0, max(self.VALS["Acc"])))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            ax=plt.subplot(2,1,2)
            plt.grid(b=True, which="both", axis="both")
            plt.plot(self.VALS["Acc"], color="red", alpha=0.3, linestyle="solid")
            if avg:
                plt.plot(np.convolve(self.VALS["avg. Acc"], np.ones((smoothing_mul,))/smoothing_mul, mode='full'), color="royalblue", alpha=0.9, linestyle="solid")
                ax.set_title("zoomed original and averaged Accuracy")
            else:
                ax.set_title("zoomed Accuracy")
            plt.xlim((0, len(self.VALS["Acc"])))
            plt.ylim((bounds[0], bounds[1]))
            ax.legend(legendstr, bbox_to_anchor=(1,0.5), loc="center left")
            
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        
        return fig

################################################
################################################
################################################
        
def cov(ins, rowvar=False):
    # torch implementation for covariance matrix
    if ins.dim() > 2:
        raise ValueError('m has more than 2 dimensions')
    if ins.dim() < 2:
        ins = ins.view(1, -1)
    if not rowvar and ins.size(0) != 1:
        ins = ins.t()
    fact = 1.0 / (ins.size(1) - 1)
    ins -= torch.mean(ins, dim=1, keepdim=True)
    inst = ins.t()
    return fact * ins.matmul(inst).squeeze()


def ZCA(ins, eps=0.1):
    # ins shape: [B, C, W, H]
    # out shape: [B, C, W, H]
    device=ins.device
    
    #rescale input
    mins=torch.min(torch.min(ins, dim=2).values).to(device)
    maxs=torch.max(torch.max(ins, dim=2).values).to(device)
    ins_scaled=(ins-mins)/(maxs-mins)
    
    # normalize input
    ins_norm = ins_scaled - torch.mean(ins_scaled, dim=0)
    
    # reshape input to vector
    ins_norm=ins_norm.view(ins.shape[0], -1)
    ins_scaled=ins_scaled.view(ins.shape[0], -1)
    
    #calculate ZCA
    covar=cov(ins_norm, rowvar=True).to(device)
    U,S,V = torch.svd(covar).to(device)
    
    retval=torch.mm(U, torch.mm(torch.eye(ins_norm.shape[0]).to(device)*(1/(torch.diag(S)+eps.to(device))), torch.mm(U.t(),ins_scaled)))

    # reshape output to image
    retval=retval.view(ins.shape[0],ins.shape[1],ins.shape[2],ins.shape[3])
    
    # rescale output
    retmins=torch.min(torch.min(retval, dim=2).values)
    retmaxs=torch.max(torch.max(retval, dim=2).values)  
    retval=(retval-retmins)/(retmaxs-retmins)
    return retval

################################################
################################################
################################################
    
def CropMatrix(b, c, w, h, cropx, cropy):
    outs=torch.ones(b,c,w,h)
    if cropy>0:
        outs[:, :, 0:cropy, :]=0
        outs[:, :, -cropy:, :]=0
    if cropx >0:
        outs[:, :, :, 0:cropx]=0
        outs[:, :, :, -cropx:]=0
    return outs

def Rotate(img, angle):
    img=torchvision.transforms.functional.to_pil_image(img)
    img=torchvision.transforms.functional.rotate(img,angle)
    img=torchvision.transforms.functional.to_tensor(img)
    return img

class AugmentMinibatch():
    def __init__(self, crop_range=False, flip_x=False, flip_y=False, shift_x=False, shift_y=False, rot_angle=False, zca=False, noise_peak=False, exponent=False, normalize=False, extend=False):
        self.CROPRANGE=crop_range
        self.FLIPX=flip_x
        self.FLIPY=flip_y
        self.SHIFTX=shift_x
        self.SHIFTY=shift_y
        self.ROTANGLE=rot_angle
        self.ZCA=zca
        self.NOISEPEAK=noise_peak
        self.EXPONENT=exponent
        self.NORMALIZE=normalize
        self.EXTEND=extend
        
    def Apply(self, ins):
        B=ins.shape[0]
        C=ins.shape[1]
        W=ins.shape[2]
        H=ins.shape[3]
        outs=ins
        
        device=ins.device
        
        if self.CROPRANGE:
            outs*=CropMatrix(B,C,W,H, cropx=random.randint(0, self.CROPRANGE), cropy=random.randint(0, self.CROPRANGE)).to(device)
            
        if self.FLIPX:
            mem=outs*1.0
            xsize=outs.shape[3]
            if random.uniform(0,1)<self.FLIPX:
                for x in range(xsize):
                    outs[:,:,:,x]=mem[:,:,:,-(x+1)]
            
        if self.FLIPY:
            mem=outs*1.0
            ysize=outs.shape[2]
            for y in range(ysize):
                outs[:,:,y,:]=mem[:,:,-(y+1),:]
                
        if self.SHIFTX:
            outs=torch.cat((outs[:,:,:,-self.SHIFTX:], outs[:,:,:,:outs.shape[3]-self.SHIFTX]), dim=3)
                
        if self.SHIFTY:
            outs=torch.cat((outs[:,:,-self.SHIFTY:,:], outs[:,:,:outs.shape[2]-self.SHIFTY,:]), dim=2)
            
        if self.ROTANGLE:
            for idx in range(B):
                angle=random.uniform(-self.ROTANGLE, self.ROTANGLE)
                outs[idx]=Rotate(outs[idx], angle).to(device)
                
        if self.ZCA:
            outs=ZCA(outs, eps=self.ZCA)
        
        if self.NOISEPEAK:
            outs+=RandTensorRange(shape=(outs.shape), minval=-self.NOISEPEAK, maxval=self.NOISEPEAK)
            
        if self.EXPONENT:
            outs=outs**self.EXPONENT
            
        if self.NORMALIZE:
            outs=(outs-outs.mean())/outs.std()
            
        if self.EXTEND:
            return torch.cat((ins, outs), dim=0)
        else:
            return outs

################################################
################################################
################################################
    
class D2toD1_class(torch.nn.Module):
    def __init__(self, num_pixels=1):
        super(D2toD1_class, self).__init__()
        self.num_pixels = num_pixels

    def forward(self, x):
        return x.view(-1, self.num_pixels)   
    
    def __repr__(self):
        return("2Dto1D(out_shape:-1, %i)"%(self.num_pixels))

################################################
################################################
################################################
    
class D1toD2_class(torch.nn.Module):
    def __init__(self, channels, x, y):
        super(D1toD2_class, self).__init__()
        self.channels=channels
        self.y=y
        self.x=x

    def forward(self, x):
        return x.view(-1, self.channels, self.y, self.x)     
    
    def __repr__(self):
        return("2Dto1D(out_shape: (-1,%i,%i,%i))"%(self.channels, self.y, self.x))   

################################################
################################################
################################################
        
def CountParameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

################################################
################################################
################################################
    
    
def Histogram(matrix, minval, maxval, bars=10, mode="percent"):
    width=(maxval-minval)/bars
    xs=width/2 + torch.arange(minval, maxval, width)
    heights=torch.zeros(bars)
    
    vector=matrix.view(-1,1).squeeze()
    vector=vector.sort(dim=0, descending=False)[0]
    
    bar=0
    idx=0
    num=vector.shape[0]
    while idx<vector.shape[0]:
        if m.isnan(vector[idx]):
            num-=1
            idx+=1
            continue
        if vector[idx]<=xs[bar]+width/2:
            heights[bar]+=1
            idx+=1
        else:
            bar+=1
    if mode=="percent":
        heights/=num
    
    return xs, heights, width

################################################
################################################
################################################
    
    
def PlotResults(nets, reporters, inputs, smoothing=5):
    print("\tPreprocessing Plot Information...")
    figurelist=[]
    figurenames=[]
    LegendString=[]
    LegendStringT=[]
    inputs=inputs.to("cpu")
    for net in nets:
        net.eval()
        net=net.to("cpu")
    
    names=[net.NAME for net in nets]
    
    alpha_low=0.4
    alpha_high=0.9
    
    for name in names:
        LegendString.append(name+" train")
        LegendString.append(name+" test")
        
    # Preprocess data
    
    xMax=0
    for reporter in reporters:
        xMax=max(reporter.VALS["Minibatch"][-1], xMax)
        
    SmoothingMuls=[1,5]
    TrainLossList=[None]*len(reporters)
    TrainLossZoomList=[None]*len(reporters)
    TestLossList=[None]*len(reporters)
    TestLossZoomList=[None]*len(reporters)
    TrainAccList=[None]*len(reporters)
    TrainAccZoomList=[None]*len(reporters)
    TestAccList=[None]*len(reporters)
    TestAccZoomList=[None]*len(reporters)
    
    xList=[None]*len(reporters)
    xZoomList=[None]*len(reporters)
    
    yMaxLoss=0
    yMinLoss=float("Inf")
    yMaxLossZoomTrain=0
    yMinLossZoomTrain=float("Inf")
    yMaxLossZoomTest=0
    yMinLossZoomTest=float("Inf")
    
    yMaxAcc=0
    yMinAcc=float("Inf")
    yMaxAccZoomTrain=0
    yMinAccZoomTrain=float("Inf")
    yMaxAccZoomTest=0
    yMinAccZoomTest=float("Inf")
    
    for netnum in range(len(reporters)):
        TrainLossList[netnum]=MovingAverage(reporters[netnum].VALS["Loss"], window=SmoothingMuls[0]*smoothing)
        TrainLossZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Loss"], window=SmoothingMuls[1]*smoothing)
        TestLossList[netnum]=MovingAverage(reporters[netnum].VALS["Test Loss"], window=SmoothingMuls[0]*smoothing)
        TestLossZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Test Loss"], window=SmoothingMuls[1]*smoothing)
        
        TrainAccList[netnum]=MovingAverage(reporters[netnum].VALS["Acc"], window=SmoothingMuls[0]*smoothing)
        TrainAccZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Acc"], window=SmoothingMuls[1]*smoothing)
        TestAccList[netnum]=MovingAverage(reporters[netnum].VALS["Test Acc"], window=SmoothingMuls[0]*smoothing)
        TestAccZoomList[netnum]=MovingAverage(reporters[netnum].VALS["Test Acc"], window=SmoothingMuls[1]*smoothing)
        
        if -SmoothingMuls[0]*smoothing+1<0:
            xList[netnum]=reporters[netnum].VALS["Minibatch"][:-SmoothingMuls[0]*smoothing+1]
        else:
            xList[netnum]=reporters[netnum].VALS["Minibatch"]
        if -SmoothingMuls[1]*smoothing+1<0:
            xZoomList[netnum]=reporters[netnum].VALS["Minibatch"][:-SmoothingMuls[1]*smoothing+1]
        else:
            xZoomList[netnum]=reporters[netnum].VALS["Minibatch"]
    
        yMaxLoss=max(yMaxLoss, max(torch.tensor(TrainLossList[netnum])[-1], torch.tensor(TestLossList[netnum])[-1]))
        yMinLoss=min(yMinLoss, min(torch.tensor(TrainLossList[netnum])[-1], torch.tensor(TestLossList[netnum])[-1]))
        yMaxLossZoomTrain=max(yMaxLossZoomTrain, torch.tensor(TrainLossZoomList[netnum])[-1])
        yMinLossZoomTrain=min(yMinLossZoomTrain, torch.tensor(TrainLossZoomList[netnum])[-1])
        yMaxLossZoomTest=max(yMaxLossZoomTest, torch.tensor(TestLossZoomList[netnum])[-1])
        yMinLossZoomTest=min(yMinLossZoomTest, torch.tensor(TestLossZoomList[netnum])[-1])
        
        yMaxAcc=max(yMaxAcc, max(torch.tensor(TrainAccList[netnum])[-1], torch.tensor(TestAccList[netnum])[-1]))
        yMinAcc=min(yMinAcc, min(torch.tensor(TrainAccList[netnum])[-1], torch.tensor(TestAccList[netnum])[-1]))
        yMaxAccZoomTrain=max(yMaxAccZoomTrain, torch.tensor(TrainAccZoomList[netnum])[-1])
        yMinAccZoomTrain=min(yMinAccZoomTrain, torch.tensor(TrainAccZoomList[netnum])[-1])
        yMaxAccZoomTest=max(yMaxAccZoomTest, torch.tensor(TestAccZoomList[netnum])[-1])
        yMinAccZoomTest=min(yMinAccZoomTest, torch.tensor(TestAccZoomList[netnum])[-1])
    
    d_val=1.5
    yLossMax=yMaxLoss*1.1
    difftrain=max(d_val*(yMaxLossZoomTrain-yMinLossZoomTrain)/2, 0.05)
    difftest=max(d_val*(yMaxLossZoomTest-yMinLossZoomTest)/2, 0.05)
    yLossZoomTrainMax=min((yMaxLossZoomTrain+yMinLossZoomTrain)/2+difftrain, 10)
    yLossZoomTrainMin=max((yMaxLossZoomTrain+yMinLossZoomTrain)/2-difftrain, -0.05)
    yLossZoomTestMax=min((yMaxLossZoomTest+yMinLossZoomTest)/2+difftest, 10)
    yLossZoomTestMin=max((yMaxLossZoomTest+yMinLossZoomTest)/2-difftest, -0.05)
    
    yAccMax=yMaxAcc*1.1
    difftrain=max(d_val*(yMaxAccZoomTrain-yMinAccZoomTrain)/2, 0.5)
    difftest=max(d_val*(yMaxAccZoomTest-yMinAccZoomTest)/2, 0.5)
    yAccZoomTrainMax=min((yMaxAccZoomTrain+yMinAccZoomTrain)/2+difftrain, 100.5)
    yAccZoomTrainMin=max((yMaxAccZoomTrain+yMinAccZoomTrain)/2-difftrain, -1)
    yAccZoomTestMax=min((yMaxAccZoomTest+yMinAccZoomTest)/2+difftest, 101)
    yAccZoomTestMin=max((yMaxAccZoomTest+yMinAccZoomTest)/2-difftest, -1)
    print("\tPreprocessing Plot Information -DONE-")
    print("\tPlotting Loss...")
    # Loss Plots
    fig=plt.figure(figsize=(12,12))
    fig.suptitle("Train and Testing Losses")
    
    ax=plt.subplot(3,1,1)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xList[netnum], TrainLossList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xList[netnum], TestLossList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("0 to %.2f"%(yLossMax))
    plt.xlim((0, xMax))
    plt.ylim((0, yLossMax))
    
    ax=plt.subplot(3,1,2)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yLossZoomTrainMin, yLossZoomTrainMax))
    plt.xlim((0, xMax))
    plt.ylim((yLossZoomTrainMin, yLossZoomTrainMax))
    
    ax=plt.subplot(3,1,3)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestLossZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yLossZoomTestMin, yLossZoomTestMax))
    plt.xlim((0, xMax))
    plt.ylim((yLossZoomTestMin, yLossZoomTestMax))
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    
    figurelist.append(fig)
    figurenames.append("Losses")
    print("\tPlotting Loss -DONE-")
    print("\tPlotting Accuracy...")
    
    # Accuracy Plots
    fig=plt.figure(figsize=(12,12))
    fig.suptitle("Train and Testing Accuracies")
    
    ax=plt.subplot(3,1,1)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xList[netnum], TrainAccList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xList[netnum], TestAccList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("0% to 100% Accuracy")
    plt.xlim((0, xMax))
    plt.ylim((0, 105))
    
    ax=plt.subplot(3,1,2)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yAccZoomTrainMin, yAccZoomTrainMax))
    plt.xlim((0, xMax))
    plt.ylim((yAccZoomTrainMin, yAccZoomTrainMax))
    
    ax=plt.subplot(3,1,3)
    plt.grid(b=True, which="both", axis="both")
    for netnum in range(len(reporters)):
        plt.plot(xZoomList[netnum], TrainAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_low, linestyle="solid")
        plt.plot(xZoomList[netnum], TestAccZoomList[netnum], color=_COLORS[netnum], alpha=alpha_high, linestyle="dashed")
    ax.legend(LegendString, bbox_to_anchor=(1, 0.5), loc="center left")
    ax.set_title("%.3f to %.3f"%(yAccZoomTestMin, yAccZoomTestMax))
    plt.xlim((0, xMax))
    plt.ylim((yAccZoomTestMin, yAccZoomTestMax))
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    
    figurelist.append(fig)
    figurenames.append("Accuracies")
    print("\tPlotting Accuracy -DONE-")
    print("\tPlotting Training Times...")
#     Time Plots
    TrainTimes=[]
    for reporter in reporters:
        TrainTimes.append(reporter.DUMPDICT["SecPerEpoch"])
    TrainTimesSorted=sorted(TrainTimes)
    NamesSorted=[x for _, x in sorted(zip(TrainTimes, names))]
    ColorsSorted=[x for _, x in sorted(zip(TrainTimes, _COLORS))]
    
    for netnum in range(len(reporters)):
        LegendStringT.append(NamesSorted[netnum])
        
    fig=plt.figure(figsize=(12,8))
    plt.grid(b=True, which="both", axis="both")

    for netnum in range(len(names)):
        plt.bar(LegendStringT[netnum], TrainTimesSorted[netnum], color=ColorsSorted[netnum], edgecolor=ColorsSorted[netnum])
    fig.suptitle("Training times\n sec / Epoch")
    
    fig.tight_layout(rect=[0, 0.03, 1, 0.9])
    
    figurelist.append(fig)
    figurenames.append("TrainTimes")
    print("\tPlotting Training Times -DONE-")
    print("\tPlotting Masking Kernels...")


    # Custom Kernel Plots
    netidx=0
    for net in nets:
        layeridx=0
        for layer in net.SEQUENCE:
            if isinstance(layer, cConv2d):
                num=layer.mask.shape[0]*layer.mask.shape[1]
                left=min(num, 300)
                
                x=min(m.ceil(left**0.5), 10)
                y=min(m.ceil(left/x), 10)
                scale=3
                
                kernelnum=0
                channelnum=0
                position=1
                plotnum=0
                fig=plt.figure(figsize=(x*scale, 2*y*scale))
                while left:
                    mask=layer.mask[kernelnum, channelnum].detach().squeeze().cpu()
                    weight=layer.weight[kernelnum, channelnum].detach().squeeze().cpu()
                    
                    ax=plt.subplot(2*y, x, position)
                    plt.imshow(torch.sigmoid(mask), cmap="Greys", vmin=0, vmax=1)
                    ax.set_title("mask  s: %.2f\nL: %i | C: %i K: %i"%(torch.sum(torch.sigmoid(mask)), layeridx, channelnum, kernelnum+channelnum*layer.mask.shape[1]))
                    
                    ax=plt.subplot(2*y, x, position+y)
                    plt.imshow(torch.sigmoid(mask)*weight, cmap="Greys")
                    ax.set_title("mask*weight\nC: %i K: %i"%(channelnum, kernelnum))
                    if kernelnum==layer.mask.shape[0]-1:
                        channelnum+=1
                        kernelnum=0
                    else:
                        kernelnum+=1
                    if position%y==0:
                        position+=y+1
                    else:
                        position+=1
                    
                    left-=1
                    if position==x*2*y+1 and left:
                        x=min(m.ceil(left**0.5), 10)
                        y=min(m.ceil(left/x), 10)
                        position=1
                        fig.tight_layout(rect=[0, 0, 1, 0.95])
                        figurelist.append(fig)
                        figurenames.append("masks/net"+str(netidx)+"/mask_net"+str(netidx)+"-layer"+str(layeridx)+"part"+str(plotnum))
                        plotnum+=1
                        fig=plt.figure(figsize=(x*scale, 2*y*scale))
                    if not left:
                        fig.tight_layout(rect=[0, 0, 1, 0.95])
                        figurelist.append(fig)
                        figurenames.append("masks/net"+str(netidx)+"/mask_net"+str(netidx)+"-layer"+str(layeridx)+"part"+str(plotnum))
            layeridx+=1
        netidx+=1
    print("\tPlotting Masking Kernels -DONE-")
    print("\tPlotting Feature Maps...")
    
    # feature maps
    netidx=0
    for net in nets:
        layeridx=0
        # outdata=inputs.to(_DEVICE)
        outdata=inputs
        for layer in net.SEQUENCE:
            outdata=layer(outdata)
            if isinstance(layer, cConv2d):
                num=layer.mask.shape[0]*layer.mask.shape[1]
                left=min(num, 100)
                
                x=min(m.ceil(left**0.5), 10)
                y=min(m.ceil(left/x), 10)
                scale=3
                kernelnum=0
                channelnum=0
                position=1
                plotnum=0
                fig=plt.figure(figsize=(x*scale, y*scale))
                while left:
                    ax=plt.subplot(y, x, position)
                    plt.imshow(outdata[channelnum, kernelnum].cpu().detach(), cmap="Greys")
                    ax.set_title("L: %i | C: %i K: %i"%(layeridx, channelnum, kernelnum+channelnum*layer.mask.shape[1]))

                    if kernelnum==layer.mask.shape[0]-1:
                        channelnum+=1
                        kernelnum=0
                    else:
                        kernelnum+=1
                        
                    position+=1
                    left-=1
                    if position==x*y+1 and left:
                        x=min(m.ceil(left**0.5), 10)
                        y=min(m.ceil(left/x), 10)
                        position=1
                        fig.tight_layout(rect=[0, 0, 1, 0.95])
                        figurelist.append(fig)
                        figurenames.append("masks/net"+str(netidx)+"/feature_net"+str(netidx)+"-layer"+str(layeridx)+"part"+str(plotnum))
                        plotnum+=1
                        fig=plt.figure(figsize=(x*scale, y*scale))
                    if not left:
                        fig.tight_layout(rect=[0, 0, 1, 0.95])
                        figurelist.append(fig)
                        figurenames.append("masks/net"+str(netidx)+"/feature_net"+str(netidx)+"-layer"+str(layeridx)+"part"+str(plotnum))
            layeridx+=1
        netidx+=1
    print("\tPlotting Feature Maps -DONE-")
    print("\tPlotting Masking Distributions...")
    
    # plots of masking  distribution (start and end)
    fig=plt.figure(figsize=(8,3*len(nets)))
    for netnum in range(len(nets)):
        net=nets[netnum]
        ax=plt.subplot(len(nets), 1, netnum+1)
        layernum=0
        for layer in net.SEQUENCE:
            if isinstance(layer, cConv2d):
                old=torch.sigmoid(net.MASKS[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.mask.view(1,-1).detach().cpu())
                
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                layernum+=1
        ax.set_title("net "+str(netnum))
        ax.legend(["Initial", "Learned"], bbox_to_anchor=(1, 0.5), loc="center left") 
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    figurelist.append(fig)
    figurenames.append("masks/mask_distributions")
    print("\tPlotting Masking Distributions -DONE-")
    
    print("\tPlotting Output Distributions for LL...")
    
    fig=plt.figure(figsize=(12, 3*len(nets)))
    axes=[]
    hmax=0
    for netnum in range(len(nets)):
        net=nets[netnum]
        
        layernum=0
        for layer in net.SEQUENCE:
            if isinstance(layer, LLAN):
                ax=plt.subplot(len(nets), 3, 3*netnum+2)
                axes.append(ax)
                old=torch.sigmoid(net.LLA[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.A.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, A"%netnum)
                plt.grid(b=True, which="both", axis="both")
                
                ax=plt.subplot(len(nets), 3, 3*netnum+3)
                axes.append(ax)
                old=torch.sigmoid(net.LLO[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.O.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, O"%netnum)
                plt.grid(b=True, which="both", axis="both")
                layernum+=1
            elif isinstance(layer, LLNAN):
                ax=plt.subplot(len(nets), 3, 3*netnum+1)
                axes.append(ax)
                old=torch.sigmoid(net.LLI[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.I.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, I"%netnum)
                plt.grid(b=True, which="both", axis="both")
                
                ax=plt.subplot(len(nets), 3, 3*netnum+2)
                axes.append(ax)
                old=torch.sigmoid(net.LLI[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.I.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, A"%netnum)
                plt.grid(b=True, which="both", axis="both")
                
                ax=plt.subplot(len(nets), 3, 3*netnum+3)
                axes.append(ax)
                old=torch.sigmoid(net.LLI[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.I.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, O"%netnum)
                plt.grid(b=True, which="both", axis="both")
            elif isinstance(layer, LLAO):
                ax=plt.subplot(len(nets), 3, 3*netnum+1)
                axes.append(ax)
                old=torch.sigmoid(net.LLI[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.I.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, I"%netnum)
                plt.grid(b=True, which="both", axis="both")
                
                ax=plt.subplot(len(nets), 3, 3*netnum+2)
                axes.append(ax)
                old=torch.sigmoid(net.LLI[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.I.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, A"%netnum)
                plt.grid(b=True, which="both", axis="both")
                
                ax=plt.subplot(len(nets), 3, 3*netnum+3)
                axes.append(ax)
                old=torch.sigmoid(net.LLI[layernum].view(1,-1).detach().cpu())
                new=torch.sigmoid(layer.I.view(1,-1).detach().cpu())
                xo,ho,wo=Histogram(old, minval=0, maxval=1, mode="percent", bars=100)
                xn,hn,wn=Histogram(new, minval=0, maxval=1, mode="percent", bars=100)
                hmax=max(max(hmax, torch.max(ho)), torch.max(hn))
                plt.bar(xo, ho, wo, alpha=0.4, color=_COLORS[layernum])
                plt.bar(xn, hn, wn, alpha=0.8, color=_COLORS[layernum])
                ax.set_title("net %i, O"%netnum)
                plt.grid(b=True, which="both", axis="both")
                layernum+=1
    for ax in axes:
        ax.set_ylim(0, hmax)
    fig.tight_layout(rect=[0, 0, 1, 0.95])
    figurelist.append(fig)
    figurenames.append("LL/parameter_distributions")
    print("\tPlotting Output Distributions for Softmax and LL -DONE-")
    
    
    
    return figurelist, figurenames

################################################
################################################
################################################
    

def SavePlots(folder, plots, names, dpi=100):
    for fignum in range(len(plots)):
        fig=plots[fignum]
        name=names[fignum]
        if fig and name:
            if name[-4:]==".png":
                fullpath=folder+"/"+name
            else:
                fullpath=folder+"/"+name+".png"
            foldername=fullpath[:fullpath.rfind("/")]
            if not os.path.exists(foldername):
                os.makedirs(foldername)
            fig.savefig(folder+"/"+name, dpi=200)
            print("%s saved."%(name))


    
    
    
    
    