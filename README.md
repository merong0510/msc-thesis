# Master's thesis

## An Identification Methodology for Industrial Fault Diagnosis with Permutation Learning in Convolutional Neural Network

###### Faculty of Electrical Engineering
###### Department of Automation Technology

Author: Jinwoo Kim.\
Submitted on January 31st, 2020.\
Thesis written in Latex.

[Model diagram of PCNN. (Permutation Convolutional Neural Network)](doc/Diagram1.png)